const fs = require('fs');
const path = require('path');

/**
 * Scales a value between two bounds relative to the desired bounds
 * @param {number} value
 * @param {number} min1
 * @param {number} max1
 * @param {number} min2
 * @param {number} max2
 * @returns {*}
 */
const map = (value, min1, max1, min2, max2) => (((value - min1) / (max1 - min1)) * (max2 - min2)) + min2;

/**
 * @param {string} flag
 * @returns {boolean}
 */
const hasFlag = (flag) => process.argv.includes(flag)

/**
 * Create folder
 * @param {string} dirPath
 * @returns {Promise<void>}
 */
const createDirectoryAsync = (dirPath) => new Promise((res, rej) => {
	fs.mkdir(dirPath, err => err ? rej(err) : res())
});

/**
 * Returns exiting of file
 * @param {string} filePath
 * @returns {Promise<boolean>}
 */
const getIsExist = filePath => new Promise(res => {
	fs.access(filePath, err => err ? res(false) : res(true));
});

/**
 * Returns is file
 * @param filePath
 * @returns {Promise<boolean>}
 */
const isFileAsync = filePath => new Promise((res, rej) => {
	fs.lstat(filePath, (err, stat) => err ? rej(err) : res(stat.isFile()));
});

/**
 * Returns size of file in bytes
 * @param {string} filePath
 * @returns {Promise<number>}
 */
const getFileSizeAsync = filePath => new Promise((res, rej) => {
	fs.stat(filePath, (err, stats) => err ? rej(err) : res(stats.size));
});

/**
 * Returns names of files that folder contains
 * @param {string} dirPath
 * @returns {Promise<string[]>}
 */
const readDirAsync = dirPath => new Promise((res, rej) => {
	fs.readdir(dirPath, (err, items) => err ? rej(err) : res(items));
});

/**
 * Returns text content from file
 * @param {string} filePath
 * @param {string} encode
 * @returns {Promise<string>}
 */
const readFileAsync = (filePath, encode = 'utf8') => new Promise((res, rej) => {
	fs.readFile(filePath, encode, (err, content) => err ? rej(err) : res(content))
});

/**
 * Write text to text
 * @param {string} filePath
 * @param {string} data
 * @param {string} encode
 * @returns {Promise<void>}
 */
const writeFileAsync = (filePath, data, encode = 'utf8') => new Promise((res, rej) => {
	fs.writeFile(filePath, data, encode, (err, content) => err ? rej(err) : res())
});

/**
 * Getting size of folder with content in bytes
 * @param dirPath
 * @returns {Promise<number>}
 */
const getFolderSizeAsync = async dirPath => {
	let sum = 0;

	const dirContent = await readDirAsync(dirPath);

	for (const contentPath of dirContent) {
		const absolutePath = path.resolve(dirPath, contentPath);

		if (await isFileAsync(absolutePath)) {
			sum += await getFileSizeAsync(absolutePath);
		} else {
			sum += await getFolderSizeAsync(absolutePath);
		}
	}

	return sum;
}

/**
 * Delete file
 * @param {string} filePath
 * @returns {Promise<void>}
 */
const deleteFileAsync = (filePath) => new Promise((res, rej) => {
	fs.unlink(filePath, err => err ? rej(err) : res())
});

/**
 *
 * @param {string} dirPath
 * @return {Promise<void>}
 */
const removeDirAsync = dirPath => new Promise((res, rej) => {
	fs.rmdir(dirPath, err => err ? rej(err) : res())
})

/**
 * Delete folder with content
 * @param {string} dirPath
 * @returns {Promise<void>}
 */
const deleteFolderAsync = async dirPath => {
	const folderContent = await readDirAsync(dirPath);

	for (const fileName of folderContent) {
		const filePath = path.resolve(dirPath, fileName);
		if (await isFileAsync(filePath)) {
			await deleteFileAsync(filePath);
		} else {
			await deleteFolderAsync(filePath);
		}
	}

	await removeDirAsync(dirPath);
}

/**
 * Handler of main execute returns
 * @param {Promise} mainReturns
 * @return void
 */
const mainWrap = (mainReturns) => {
	mainReturns
		.then(() => {
			process.exit(0);
		})
		.catch(err => {
			console.error(err);
			process.exit(1);
		})
}

module.exports = {
	getIsExist,
	isFileAsync,
	getFileSizeAsync,
	readDirAsync,
	readFileAsync,
	writeFileAsync,
	getFolderSizeAsync,
	map,
	createDirectoryAsync,
	deleteFileAsync,
	deleteFolderAsync,
	hasFlag,
	mainWrap,
}