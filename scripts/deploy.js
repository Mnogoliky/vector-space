const path = require('path');
const ftp = require('basic-ftp');
const cliProgress = require('cli-progress');
const request = require('request');

const {
	getFolderSizeAsync,
	getIsExist,
	mainWrap,
	isFileAsync,
	getFileSizeAsync,
} = require('./functions');

const FTP_HOST = 'h18.netangels.ru';
const FTP_USER = 'c25908_vectorspace_app_folder';
const FTP_PASSWORD = 'IZG7GESZGrqf24Uz';

const getClient = async () => {
	try {
		const client = new ftp.Client();
		client.ftp.verbose = false;
		
		await client.access({
			host: FTP_HOST,
			user: FTP_USER,
			password: FTP_PASSWORD,
			secure: false,
		});
		
		return client;
	} catch (e) {
		console.error(e);
		process.exit(2);
	}
}

const restartServerAsync = () => new Promise((resolve, reject) => {
	request('http://vectorspace.gredactor.ru/api/restart', (error, response, body) => {
		if (!error && response.statusCode === 200) {
			resolve(body);
		} else {
			reject();
		}
	})
});

const foldersList = [
	// './css',
	'./dist',
	// './img',
	'./js',
	// './index.html',
	// './favicon.ico',
	// './index.js',
	// './package.json',
	// './yarn.lock',
];

const main = async () => {
	try {
		const client = await getClient();
		
		let bytesToUpload = 0;

		for (const filePath of foldersList) {
			const absolutePath = path.resolve(__dirname, '../', filePath);

			if (await isFileAsync(absolutePath)) {
				bytesToUpload += await getFileSizeAsync(absolutePath);
			} else {
				bytesToUpload += await getFolderSizeAsync(absolutePath);
			}
		}
		
		for (const filePath of foldersList) {

			if (await isFileAsync(filePath)) {
				try {
					await client.remove(filePath);
				} catch (e) {
					console.error(`Remote file ${filePath} is not exist`);
				}
			} else {
				try {
					await client.removeDir(filePath);
				} catch (e) {
					console.error(`Remote directory ${filePath} is not exist`);
				}
			}
		}

		const progressBar = new cliProgress.Bar({}, cliProgress.Presets.shades_classic);

		progressBar.start(bytesToUpload, 0);

		client.trackProgress(info => {
			progressBar.update(info.bytesOverall);
		});

		for (const filePath of foldersList) {
			const absolutePath = path.resolve(__dirname, '../', filePath);

			if (await getIsExist(absolutePath)) {
				if (await isFileAsync(absolutePath)) {
					await client.uploadFrom(absolutePath, filePath);
				} else {
					await client.uploadFromDir(absolutePath, filePath);
				}
			} else {
				console.error(`File/directory ${filePath} is not exist`);
			}
		}

		progressBar.stop();

		await restartServerAsync();

		process.exit(0);
	} catch (e) {
		console.error(e);
		process.exit(1);
	}
}

mainWrap(main());