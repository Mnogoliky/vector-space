

let num = 0;
let hash = '';

const objectToBody = (obj) => Object.keys(obj).map((key) => `${key}=${obj[key]}`).join('&');

const wait = time => new Promise((resolve => setTimeout(resolve,time)));

const OpenWebSocket = url =>
		new Promise((resolve, reject) => {
			const connection = new WebSocket(url);
			connection.onopen = () => resolve(connection);
			connection.onerror = reject;
});

const connector = async () => {
	let name = localStorage.getItem('name');
	if (name === null)
	{
		name = prompt('name','Player');
		localStorage.setItem('name',name);
	}
	const response = await fetch('/api/connect?name='+name).then(r => r.json());
	hash = response.hash;
	num = response.num;
	console.log(response);
	await getMap();
};

const getMap = async () => {
	let response = {};
	const connection = await OpenWebSocket(`ws://${location.host}/ws`);

	let wh = true;
	let lastData = '';

	connection.onmessage = e => {
		const data = JSON.parse(e.data);
		objects = data.objects || objects;
		plasma = data.plasma || plasma;
		num = data.num || num;

		 // const name = 'map';
		 // animator.canvas[name].camera.image(animator.canvas[name].camera, animator.canvas[name])

	};

	connection.onclose = () => {
		wh = false;
		Message('router', 'Disconnect');
	};

	while(wh)
	{
		const newData = JSON.stringify({
				hash, controls:
					{
						forward: Boolean(keys[control.forward]),
						turbo: Boolean(keys[control.turbo]),
						fire: Boolean(keys[control.shot]),
						fire2: Boolean(keys[control.shot2]),
						backward: Boolean(keys[control.backward]),
						turnRight: Boolean(keys[control.right]),
						turnLeft: Boolean(keys[control.left]),
					}
			})
		;

		if (lastData !== newData)
		{
			connection.send(newData);
			lastData = newData;
		}
		await wait(10);
	}



};

/*

		const body = {hash, controls: JSON.stringify(new GetControlObject)};
		response = await fetch('/api/map?'+objectToBody(body)).then(r => r.json());
		objects = response.objects || objects;
		plasma = response.plasma || plasma;
		num = response.num || num;

 */