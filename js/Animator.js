let full = false;
let full_elem;
let full_width;
let full_height;
let full_pos;

let videoSpeed = 30;

const animator =
		{
			'canvas': {},
		};

function Map(inn, min1, max1, min2, max2) {
	return (((inn - min1) / (max1 - min1)) * (max2 - min2)) + min2;
}

let cameraFunctions = {
	MT(x, y) {
		this.moveTo(Map(x - this.camera.x, 0, animator.canvas.map.camera.w, 0, animator.canvas.map.width), Map(y - this.camera.y, 0, animator.canvas.map.camera.h, 0, animator.canvas.map.height));
	},
	TitleMove(x, y, w) {
		this.moveTo(Map(x - this.camera.x, 0, animator.canvas.map.camera.w, 0, animator.canvas.map.width) + (w / 2), Map(y - this.camera.y, 0, animator.canvas.map.camera.h, 0, animator.canvas.map.height));
	},
	TitleLine(x, y, w) {
		this.moveTo(Map(x - this.camera.x, 0, animator.canvas.map.camera.w, 0, animator.canvas.map.width) + (w / 2), Map(y - this.camera.y, 0, animator.canvas.map.camera.h, 0, animator.canvas.map.height));
	},
	LT(x, y) {
		this.lineTo(Map(x - this.camera.x, 0, animator.canvas.map.camera.w, 0, animator.canvas.map.width), Map(y - this.camera.y, 0, animator.canvas.map.camera.h, 0, animator.canvas.map.height));
	},
	Star(x, y) {
		this.fillRect(Map(x - this.camera.x, 0, animator.canvas.map.camera.w, 0, animator.canvas.map.width), Map(y - this.camera.y, 0, animator.canvas.map.camera.h, 0, animator.canvas.map.height), 1, 1);//(animator.canvas.map.width/animator.canvas.map.camera.w),(animator.canvas.map.height/animator.canvas.map.camera.h));
	},
	FRect(x, y, w, h) {
		this.fillRect(Map(x - Math.round(this.camera.x), 0, animator.canvas.map.camera.w, 0, animator.canvas.map.width), Map(y - Math.round(this.camera.y), 0, animator.canvas.map.camera.h, 0, animator.canvas.map.height), w * (animator.canvas.map.width / animator.canvas.map.camera.w), h * (animator.canvas.map.height / animator.canvas.map.camera.h));
	},
	SRect(x, y, w, h) {
		this.strokeRect(Map(x - Math.round(this.camera.x), 0, animator.canvas.map.camera.w, 0, animator.canvas.map.width), Map(y - Math.round(this.camera.y), 0, animator.canvas.map.camera.h, 0, animator.canvas.map.height), w * (animator.canvas.map.width / animator.canvas.map.camera.w), h * (animator.canvas.map.height / animator.canvas.map.camera.h));
	},
	progressBar(x, y, w, h, progress, color) {
		this.fillStyle = color;
		this.strokeStyle = color;
		this.SRect(Math.round(x) - 0.5, Math.round(y) - 0.5, Math.round(w), Math.round(h));
		this.FRect(Math.round(x + 1) - 0.5, Math.round(y + 1) - 0.5, ((w * (progress / 100)) - 2), Math.round(h - 2))
	},
	CText(txt, x, y) {
		this.fillText(txt, x, y);
		this.strokeText(txt, x, y);
	},
	Image(img, x, y, w, h) {
		this.drawImage(animator[img], x, y, w, h);
	},
	Text(txt, x, y) {
		this.fillText(txt, Map(x - this.camera.x, 0, animator.canvas.map.camera.w, 0, animator.canvas.map.width), Map(y - this.camera.y, 0, animator.canvas.map.camera.h, 0, animator.canvas.map.height));
	},
};

animator.add = function (name, id) {
	animator.canvas[name] = document.getElementById(id);
	animator.canvas[name].ctx = document.getElementById(id).getContext('2d');
};

animator.addImage = function (name, id) {
	this[name] = new Image();
	this[name].src = id;
};

animator.start = function () {
	for(let name in animator.canvas) {
		const currentCanvas = animator.canvas[name];

		if (animator.canvas[name].camera === undefined) {
			animator.canvas[name].ctx.textBaseline = 'top';
			animator.canvas[name].ctx.fillText('camera not found', 0, 0);
		}
		else
			{
			for(let fun in cameraFunctions)
				animator.canvas[name].ctx[fun] = cameraFunctions[fun];
			animator.canvas[name].ctx.camera = animator.canvas[name].camera;

			if (animator.canvas[name].camera.image === undefined) {
				animator.canvas[name].ctx.textBaseline = 'top';
				animator.canvas[name].ctx.fillText('camera invalid', 0, 0);
			}
			 else
			 	animator.canvas[name].run = setInterval(function () {
			 		//animator.canvas[name].ctx.clearRect(0, 0, animator.canvas[name].width, animator.canvas[name].height);
			 		//animator.canvas[name].ctx.drawImage(animator.canvas[name].camera.image(animator.canvas[name].camera), 0, 0, animator.canvas[name].width, animator.canvas[name].height);
			 		//phisik();
			 		animator.canvas[name].camera.image(animator.canvas[name].camera, animator.canvas[name])
			 	}, (1000 / videoSpeed));
		}
	}
};

function Full(name) {
	if (full) {
		full_elem.style.width = full_width;
		full_elem.style.height = full_height;
		full_elem.style.position = full_pos;

		if (document.cancelFullScreen) {
			document.cancelFullScreen();
		}
		else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		}
		else if (document.webkitCancelFullScreen) {
			document.webkitCancelFullScreen();
		}
	}
	else {
		full_elem = animator.canvas[name];
		full_width = animator.canvas[name].style.width;
		full_height = animator.canvas[name].style.height;
		full_pos = animator.canvas[name].style.position;
		full_elem.style.width = '100%';
		full_elem.style.height = '100%';
		full_elem.style.position = 'fixed';

		if (animator.canvas[name].requestFullScreen) {
			animator.canvas[name].requestFullScreen();
		}
		else if (animator.canvas[name].mozRequestFullScreen) {
			animator.canvas[name].mozRequestFullScreen();
		}
		else if (animator.canvas[name].webkitRequestFullScreen) {
			animator.canvas[name].webkitRequestFullScreen();
		}
	}

	full = !full;
}