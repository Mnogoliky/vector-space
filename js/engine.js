var objectPhisik = {
	'sound':function(object){
		objects[object].clear = objects[object].sound.ended;
	},
	'rocket':function(n){
		if (objects[n].controls.forward)
		{
			if (objects[n].resurs.fuel[0] > 1)
			{
				for (var i = 0; i < 10; i++)
				{
					var s = {'x':0,'y':0,'sx':10,'sy':10,'sz':10,'size':10,'color':'orange'};
					s.sz = rand(6,10);
					s.color = 'rgb('+rand(230,255)+','+rand(140,160)+',0)';
					s.x = objects[n].x + (Math.cos((models[objects[n].model].bracings[0][0]+objects[n].d)*0.0175)*models[objects[n].model].bracings[0][1]) + rand(-15,15);
					s.y = objects[n].y + (Math.sin((models[objects[n].model].bracings[0][0]+objects[n].d)*0.0175)*models[objects[n].model].bracings[0][1]) + rand(-15,15);

					s.sx = (Math.cos((models[objects[n].model].bracings[0][2]+objects[n].d + rand(-15,15))*0.0175)*objects[n].speed);
					s.sy = (Math.sin((models[objects[n].model].bracings[0][2]+objects[n].d + rand(-15,15))*0.0175)*objects[n].speed);

					s.ss = Math.abs(s.sx)+Math.abs(s.sy);
					particles.push(s);
				}

				for (var i = 0; i < 10; i++)
				{
					var s = {'x':0,'y':0,'sx':10,'sy':10,'sz':10,'size':10,'color':'orange'};
					s.sz = rand(6,10);
					s.x = objects[n].x + (Math.cos((models[objects[n].model].bracings[1][0]+objects[n].d)*0.0175)*models[objects[n].model].bracings[1][1]) + rand(-15,15);
					s.y = objects[n].y + (Math.sin((models[objects[n].model].bracings[1][0]+objects[n].d)*0.0175)*models[objects[n].model].bracings[1][1]) + rand(-15,15);

					s.sx = (Math.cos((models[objects[n].model].bracings[1][2]+objects[n].d + rand(-15,15))*0.0175)*objects[n].speed);
					s.sy = (Math.sin((models[objects[n].model].bracings[1][2]+objects[n].d + rand(-15,15))*0.0175)*objects[n].speed);

					s.ss = Math.abs(s.sx)+Math.abs(s.sy);
					particles.push(s);
				}


				objects[n].sx += (Math.cos(objects[n].d*0.0175)*objects[n].speed) * (10/objects[n].mass);
				objects[n].sy += (Math.sin(objects[n].d*0.0175)*objects[n].speed) * (10/objects[n].mass);
				objects[n].resurs.fuel[0] -= 0.02;
			}
		}
		if (objects[n].controls.backward)
		{
			if (objects[n].resurs.fuel[0] > 1)
			{
				for (var i = 0; i < 5; i++)
				{
					var fakeDir = rand(-200,200)/100;
					var s = {'x':0,'y':0,'sx':10,'sy':10,'sz':10,'size':10,'color':'orange'};
					s.sz = rand(3,6);
					s.x = objects[n].x + (Math.cos((models[objects[n].model].bracings[2][0]+objects[n].d)*0.0175)*models[objects[n].model].bracings[2][1]) + rand(-5,5);
					s.y = objects[n].y + (Math.sin((models[objects[n].model].bracings[2][0]+objects[n].d)*0.0175)*models[objects[n].model].bracings[2][1]) + rand(-5,5);

					s.sx = (models[objects[n].model].bracings[2][2]+Math.cos((objects[n].d+fakeDir)*0.0175)*rand(0,15));
					s.sy = (models[objects[n].model].bracings[2][2]+Math.sin((objects[n].d+fakeDir)*0.0175)*rand(0,15));

					s.ss = Math.abs(s.sx)+Math.abs(s.sy);
					particles.push(s);
				}

				for (var i = 0; i < 5; i++)
				{
					var fakeDir = rand(-200,200)/100;
					var s = {'x':0,'y':0,'sx':10,'sy':10,'sz':10,'size':10,'color':'orange'};
					s.sz = rand(6,10);
					s.x = objects[n].x + (Math.cos((models[objects[n].model].bracings[3][0]+objects[n].d)*0.0175)*models[objects[n].model].bracings[3][1]) + rand(-5,5);
					s.y = objects[n].y + (Math.sin((models[objects[n].model].bracings[3][0]+objects[n].d)*0.0175)*models[objects[n].model].bracings[3][1]) + rand(-5,5);

					s.sx = (models[objects[n].model].bracings[3][2]+Math.cos((objects[n].d+fakeDir)*0.0175)*rand(0,15));
					s.sy = (models[objects[n].model].bracings[3][2]+Math.sin((objects[n].d+fakeDir)*0.0175)*rand(0,15));

					s.ss = Math.abs(s.sx)+Math.abs(s.sy);
					particles.push(s);
				}

				objects[n].sx += (Math.cos((180+objects[n].d)*0.0175)*objects[n].speed) * (5/objects[n].mass);
				objects[n].sy += (Math.sin((180+objects[n].d)*0.0175)*objects[n].speed) * (5/objects[n].mass);
				objects[n].resurs.fuel[0] -= 0.01;
			}
		}

		if (objects[n].controls.fire)
		{
			if (objects[n].fsave == undefined){
				objects[n].fsave = Date.now() - objects[n].ftime;
				objects[n].fn = true;
			}
			if (Date.now() - objects[n].fsave > objects[n].ftime)
			{
				objects[n].fn = !objects[n].fn;
				for(var ss = 0; ss < 1; ss++)
					if (objects[n].resurs.energy[0] > 0.1){
						var fakeDir = rand(-30,30)/20;
						var spd = Math.sqrt(Math.pow(objects[n].sx,2)+Math.pow(objects[n].sy,2));//(objects[n].speed*2)
						fire(objects[n].x+Math.cos((objects[n].d+models[objects[n].model].bracings[4+objects[n].fn][0])*0.0175)*(models[objects[n].model].bracings[4+objects[n].fn][1]+spd),objects[n].y+Math.sin((objects[n].d+models[objects[n].model].bracings[4+objects[n].fn][0])*0.0175)*(models[objects[n].model].bracings[4+objects[n].fn][1]+spd),(objects[n].d+fakeDir),500,'a',true,objects[n]);

						objects[n].resurs.energy[0] -= 0.1;
					}

				objects[n].fsave = Date.now();
			}
		}
		if (objects[n].controls.turnRight)
			if (objects[n].resurs.energy[0] > 1){
				objects[n].sd += (10 - objects[n].sd) * (7/objects[n].mass);
				objects[n].resurs.energy[0] -= 0.02;
			}

		if (objects[n].controls.turnLeft)
			if (objects[n].resurs.energy[0] > 1){
				objects[n].sd -= (10 + objects[n].sd) * (7/objects[n].mass);
				objects[n].resurs.energy[0] -= 0.02;
			}
	},
	'turel':function(n){
		if (objects[n].servo === undefined) objects[n].servo = 5;
		if (objects[n].rervo === undefined) objects[n].rervo = objects[n].servo;

		if (objects[n].controls.fire)
		{
			if (objects[n].fsave === undefined){
				objects[n].fsave = Date.now() - objects[n].ftime;
				objects[n].fn = true;
			}
			if (Date.now() - objects[n].fsave > objects[n].ftime)
			{
				objects[n].fn = !objects[n].fn;
				for(var ss = 0; ss < 1; ss++)
					if (objects[n].resurs.energy[0] > 0.1){
						var fakeDir = rand(-10,10)/20;
						var spd = Math.sqrt(Math.pow(objects[n].sx,2)+Math.pow(objects[n].sy,2));//(objects[n].speed*2)
						fire(objects[n].x+Math.cos((objects[n].d+models[objects[n].model].bracings[4+objects[n].fn][0])*0.0175)*(models[objects[n].model].bracings[4+objects[n].fn][1]+spd),objects[n].y+Math.sin((objects[n].d+models[objects[n].model].bracings[4+objects[n].fn][0])*0.0175)*(models[objects[n].model].bracings[4+objects[n].fn][1]+spd),(objects[n].d+fakeDir),250,'a',true,objects[n]);

						objects[n].resurs.energy[0] -= 0.1;
					}

				objects[n].fsave = Date.now();
			}
		}
		if (objects[n].controls.turnRight)
			if (objects[n].resurs.energy[0] > 1){
				objects[n].sd += (objects[n].rervo - objects[n].sd) * (7/objects[n].mass);
				objects[n].resurs.energy[0] -= 0.02;
			}

		if (objects[n].controls.turnLeft)
			if (objects[n].resurs.energy[0] > 1){
				objects[n].sd -= (objects[n].rervo + objects[n].sd) * (7/objects[n].mass);
				objects[n].resurs.energy[0] -= 0.02;
			}
	},
};

function controller(obj){
	if (obj.controller === 'turel'){
		var p = 0;
		var barbas = [];

		for (var p = 0; p < objects.length; p++)
			if ((objects[p].team !== undefined) && (objects[p].team !== 'white') && (objects[p].name !== obj.name))
				if ((objects[p].team === 'gray') || (objects[p].team !== obj.team))
					barbas.push(p);

		obj.target = barbas[0];

		for (var p = 0; p < barbas.length; p++)
				//	if (Long(obj.x,obj.y,objects[barbas[p]].x,objects[barbas[p]].y) < 200)
			if (Long(obj.x,obj.y,objects[barbas[p]].x,objects[barbas[p]].y) < Long(obj.x,obj.y,objects[obj.target].x,objects[obj.target].y))
				obj.target = barbas[p];

		if (obj.target !== undefined)
			obj.controls.fire = controll_diraction(obj);
		else
			controll_nocommand(obj);
	}
	else
	if (obj.controller === 'player'){
		obj.controls.forward = !!keys[control.forward];
		obj.controls.fire = !!keys[control.shot];
		obj.controls.backward = !!keys[control.backward];
		obj.controls.turnRight = !!keys[control.right];
		obj.controls.turnLeft = !!keys[control.left];
	}
}

function controll_diraction(obj){
	var dr = (obj.d+90)*0.0175//Diraction(obj.x,obj.y,objects[obj.target].x,objects[obj.target].y);
	var lg =      Long(obj.x,obj.y,objects[obj.target].x,objects[obj.target].y);
	var s = Scalar(obj.x-objects[obj.target].x,obj.y-objects[obj.target].y,Math.cos(dr)*lg,Math.sin(dr)*lg)
	var a = Math.abs(s) < 0;
	var min = Math.abs(s/(lg*lg))*57.3;
	obj.rervo = Math.min(obj.servo,(min)/15);

	if (s < 0)
	{
		obj.controls.turnRight = true;
		obj.controls.turnLeft = false;
	}
	else
	{
		obj.controls.turnRight = false;
		obj.controls.turnLeft = true;
	}
	return (min < 1);
}

function controll_nocommand(obj) {
	obj.controls.turnRight = false;
	obj.controls.turnLeft = false;
	obj.controls.fire = false;
}

function phisik(one){
	var saveTime = Date.now();

	/* plazma */

	for (var n = 0; n < plasma.length; n++)
		if ((plasma[n].s < 5) || (!StarShow(plasma[n].x,plasma[n].y)))
			plasma.splice(n, 1);

	for (var n = 0; n < plasma.length; n++){
		if (!!plasma[n].w){
			var k = Math.min(Math.max(Math.round(plasma[n].s/(quality/100)),1),20);
			for (var tt = 0; tt < k; tt++){

				for (var i = 0; i < objects.length; i++)
					if (Long(plasma[n].x+Math.cos(plasma[n].d*0.0175)*(plasma[n].s*1.5),plasma[n].y+Math.sin(plasma[n].d*0.0175)*(plasma[n].s*1.5),objects[i].x,objects[i].y) < objects[i].max+plasma[n].s+2)
						for(var j = 1; j < models[objects[i].model].sketh.length; j+=2)
							if (Intersection(plasma[n].x,plasma[n].y,plasma[n].x+Math.cos(plasma[n].d*0.0175)*plasma[n].s,plasma[n].y+Math.sin(plasma[n].d*0.0175)*plasma[n].s,(Math.cos((models[objects[i].model].sketh[j-1][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j-1][1])+objects[i].x+objects[i].sx,(Math.sin((models[objects[i].model].sketh[j-1][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j-1][1])+objects[i].y+objects[i].sy,(Math.cos((models[objects[i].model].sketh[j][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j][1])+objects[i].x+objects[i].sx,(Math.sin((models[objects[i].model].sketh[j][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j][1])+objects[i].y+objects[i].sy))
							{
								hit(i,j,plasma[n].s,plasma[n].starter);
								var l = plasma[n].s;
								while(Intersection(plasma[n].x,plasma[n].y,plasma[n].x+Math.cos(plasma[n].d*0.0175)*l,plasma[n].y+Math.sin(plasma[n].d*0.0175)*l,(Math.cos((models[objects[i].model].sketh[j-1][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j-1][1])+objects[i].x+objects[i].sx,(Math.sin((models[objects[i].model].sketh[j-1][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j-1][1])+objects[i].y+objects[i].sy,(Math.cos((models[objects[i].model].sketh[j][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j][1])+objects[i].x+objects[i].sx,(Math.sin((models[objects[i].model].sketh[j][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j][1])+objects[i].y+objects[i].sy))
								{
									l -= (Map(quality,0,100,10,1));
								}
								l -= (Map(quality,0,100,10,1));
								plasma[n].x += Math.cos(plasma[n].d*0.0175)*l;
								plasma[n].y += Math.sin(plasma[n].d*0.0175)*l;

								var svd = plasma[n].d;
								var barrier_dir = Diraction((Math.cos((models[objects[i].model].sketh[j-1][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j-1][1])+objects[i].x,(Math.sin((models[objects[i].model].sketh[j-1][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j-1][1])+objects[i].y,(Math.cos((models[objects[i].model].sketh[j][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j][1])+objects[i].x,(Math.sin((models[objects[i].model].sketh[j][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j][1])+objects[i].y);
								plasma[n].d = (barrier_dir*57.30) + ((barrier_dir*57.30) - svd);

								var r = rand(1,10) * Mirrow;
								for (var h = 0; h < r; h++)
									fire(plasma[n].x,plasma[n].y,plasma[n].d+(rand(-50,50)/10),plasma[n].s*(1/r/2),plasma[n].t,false,plasma[n].starter);
								plasma[n].s=0;

								break;
							}

				plasma[n].x += Math.cos(plasma[n].d*0.0175)*plasma[n].s*(1/k);
				plasma[n].y += Math.sin(plasma[n].d*0.0175)*plasma[n].s*(1/k);

			}
			plasma[n].s *= 0.96;
		}
		plasma[n].w = true;
	}

	/* objects */

	for (var n = 0; n < objects.length; n++){

		var max = 0;
		var mass = 0;

		for (var j = 0; j < models[objects[n].model].sketh.length; j++)
		{
			max = Math.max(max,models[objects[n].model].sketh[j][1]);
			mass += models[objects[n].model].sketh[j][1];
		}

		mass /= models[objects[n].model].sketh.length;

		objects[n].max = max;
		objects[n].mass = mass;

		objects[n].x += objects[n].sx;
		objects[n].y += objects[n].sy;
		objects[n].d += objects[n].sd;

		objects[n].sx -= objects[n].sx * (2/objects[n].mass);
		objects[n].sy -= objects[n].sy * (2/objects[n].mass);
		objects[n].sd -= objects[n].sd * (2/objects[n].mass);


		if (objectPhisik[objects[n].type] !== undefined)
			objectPhisik[objects[n].type](n);
		controller(objects[n]);

	}

	for (var n = 0; n < objects.length; n++)
		if (objects[n].clear)
			objects.splice(n,1);

	for (var n = 0; n < particles.length; n++){
		for (var i = 0; i < objects.length; i++)
			if (Long(particles[n].x+particles[n].sx,particles[n].y+particles[n].sy,objects[i].x,objects[i].y) < objects[i].max+10)
			{
				for(var j = 1; j < models[objects[i].model].sketh.length; j+=2)
					if (Intersection(particles[n].x,particles[n].y,particles[n].x+particles[n].sx,particles[n].y+particles[n].sy,(Math.cos((models[objects[i].model].sketh[j-1][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j-1][1])+objects[i].x+objects[i].sx,(Math.sin((models[objects[i].model].sketh[j-1][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j-1][1])+objects[i].y+objects[i].sy,(Math.cos((models[objects[i].model].sketh[j][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j][1])+objects[i].x+objects[i].sx,(Math.sin((models[objects[i].model].sketh[j][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j][1])+objects[i].y+objects[i].sy))
					{
						var spd = Math.sqrt(Math.pow(objects[i].sx-particles[n].sx,2)+Math.pow(objects[i].sy-particles[n].sy,2));
						var barrier_dir = Diraction((Math.cos((models[objects[i].model].sketh[j-1][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j-1][1])+objects[i].x,(Math.sin((models[objects[i].model].sketh[j-1][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j-1][1])+objects[i].y,(Math.cos((models[objects[i].model].sketh[j][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j][1])+objects[i].x,(Math.sin((models[objects[i].model].sketh[j][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j][1])+objects[i].y);
						var par_dir = Diraction(particles[n].x,particles[n].y,particles[n].x+particles[n].sx,particles[n].y+particles[n].sy);
						var newdir = barrier_dir + (barrier_dir - par_dir);

						particles[n].sx = Math.cos(newdir)*spd;
						particles[n].sy = Math.sin(newdir)*spd;
						break;
					}
			}

		particles[n].x += particles[n].sx;
		particles[n].y += particles[n].sy;

		particles[n].sx -= particles[n].sx * particlesLife;
		particles[n].sy -= particles[n].sy * particlesLife;

		particles[n].size = particles[n].sz / (particles[n].ss / (Math.abs(particles[n].sx)+Math.abs(particles[n].sy)));
	}

	for (var n = 0; n < particles.length; n++)
		if ((Math.abs(particles[n].sx)+Math.abs(particles[n].sy) < 0.5) || (!StarShow(particles[n].x,particles[n].y)))
			particles.splice(n, 1);

	for (var n = 0; n < deads.length; n++){
		deads[n].x += deads[n].sx;
		deads[n].y += deads[n].sy;
		deads[n].d += deads[n].sd;

		deads[n].sx -= deads[n].sx * 0.01;
		deads[n].sy -= deads[n].sy * 0.01;
		//deads[n].sd -= deads[n].sd * particlesLife;

		deads[n].s -= deads[n].s * particlesLife;
	}

	for (var n = 0; n < deads.length; n++)
		if (deads[n].s < 0.2)
			deads.splice(n, 1);

	if(!!keys.PgUp)
	{
		animator.canvas.map.camera.w+=(window.screen.width/10);
		animator.canvas.map.camera.h+=(window.screen.height/10);
	}
	if(!!keys.PgDn)
	{
		animator.canvas.map.camera.w-=(window.screen.width/10);
		animator.canvas.map.camera.h-=(window.screen.height/10);
	}

	//document.getElementById('outt').innerHTML = JSON.stringify(models);


	phisikTime = Date.now() - saveTime;
	if (!one)
		phisikWhile = setTimeout(phisik,Math.max(phisikHZ - phisikTime,1));
}