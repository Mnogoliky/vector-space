window.addEventListener('load', start);

let SAVE_CAMERA_X = 0;
let SAVE_CAMERA_Y = 0;

function Map(inn, min1, max1, min2, max2) {
    return (((inn - min1) / (max1 - min1)) * (max2 - min2)) + min2;
}

function rand(min, max) {
    return Math.round(Math.random() * (max - min) + min);
}

var playList = [
    ['Love Me Like You Do', 'Audio/Musik/8-Bit Universe — Love Me Like You Do (8-Bit Version).mp3'],
    ['Nirvana - Smells Like Teen Spirit', 'Audio/Musik/8-Bit Universe — Smells Like Teen Spirit (8-Bit Version).mp3'],
    ['Nirvana - Rape me', 'Audio/Musik/nirvana_rape_me_8bit_remix.mp3'],
    ['Rammstein - Mein_herz_brennt', 'Audio/Musik/rammstein_-_mein_herz_brennt_8_bit_remix.mp3'],
    ['Rammstein - Sonne', 'Audio/Musik/rammstein_-_sonne_8_bit_(zf.fm).mp3'],
    ['SOAD - Lonely day', 'Audio/Musik/SOAD - Lonely day 8bit.mp3'],
];
var soundNo = -1;
var player = new Audio();

var messTime = [0, 150, 1500, 150];
var messages = [];

var phisikTime = 0;
var phisikHZ = (1000 / 25);
var fps = 0;

var Mirrow = false; ///Временная переменная рикошет

var particlesLife = 0.005 * 40;

var quality = 10;

var pilotObject = 0;
var phisikWhile;

var starsMany = 500;

var volume = {
    'total': 0,
    'shots': 1,
    'musik': 1,
    'explosions': 1,
    'hits': 1,
};

var paused = false;

var MenuFont = 'geo';
var MenuSize = 50;
var MenuMusik = false;
var MenuBackground;
var MenuCloseId;
var menuItems = [
    ['play', 'Play', Emenu],
    ['settings', 'Settings', function () {
        clearInterval(MenuCloseId);
        MenuCloseId = setInterval(function () {
            Settings(animator.canvas.map.ctx)
        }, 10);
        ItemScript = () => {
        };
    }],
    ['full', 'Fullscreen', () => Full('map')],
];
var ItemTab = 0;
var ItemScript = function () {
};
var ItemMin;
var ItemMax;
var ItemValue;

var ItemPick = new Audio();
var ItemClick = new Audio();

var settingsItems = {
    'Video': {
        'name': 'Video settings',
        'img': 'eye',
        'Items': [
            ['Particles', 'bar', [50, 1, 40], val => {
                settingsItems.Video.Items[0][2][2] = val;
                particlesLife = (val * 0.005)
            }],
            ['Stars', 'bar', [0, 40, 40], val => {
                settingsItems.Video.Items[1][2][2] = val;
                starsMany = (val * 10)
            }],
            ['FPS', 'bar', [2, 8, 3], val => {
                settingsItems.Video.Items[2][2][2] = val;
                videoSpeed = (val * 10)
            }],
            ['Screen size', 'pick', [['1366x768', '1920x1080', '360x640', '1024x768', '1280x1024', '1600x900', '1440x900', '1280x800', '1680x1050', '1536x864'], 'Custom'], val => {
                settingsItems.Video.Items[3][2][1] = val;
                animator.canvas.map.width = (parseInt(settingsItems.Video.Items[3][2][0][val].split('x')[0]));
                animator.canvas.map.height = (parseInt(settingsItems.Video.Items[3][2][0][val].split('x')[1]));
            }],
        ]
    },
    'Calculating': {
        'name': 'Quality calculating',
        'img': 'turbo',
        'Items': [
            ['Phisik', 'bar', [1, 10, 1], function (val) {
                settingsItems.Calculating.Items[0][2][2] = val;
                quality = val * 10;
            }],
        ]
    },
    'UI': {
        'name': 'Interfase',
        'img': 'menu',
        'Items': [
            ['Size', 'bar', [1, 9, 5], function (val) {
                settingsItems.UI.Items[0][2][2] = val;
                MenuSize = (val * 10)
            }],
            ['Font', 'pick', [['geo', 'rex', 'cuprum', 'digital', 'Capital_Clickbait', 'fatal_error'], 0], function (val) {
                settingsItems.UI.Items[1][2][1] = val;
                MenuFont = settingsItems.UI.Items[1][2][0][val]
            }],
        ]
    },
    'Controls': {
        'name': 'Controls',
        'img': 'control',
        'Items': [
            ['Forward', 'key', 'W', () => {
                keyTrap = key => {
                    settingsItems.Controls.Items[0][2] = assoc[key];
                    control.forward = key
                };
                settingsItems.Controls.Items[0][2] = '..';
            }],
            ['Backward', 'key', 'S', () => {
                keyTrap = key => {
                    settingsItems.Controls.Items[1][2] = assoc[key];
                    control.backward = key
                };
                settingsItems.Controls.Items[1][2] = '..';
            }],
            ['Right', 'key', 'D', () => {
                keyTrap = key => {
                    settingsItems.Controls.Items[2][2] = assoc[key];
                    control.right = key
                };
                settingsItems.Controls.Items[2][2] = '..';
            }],
            ['Left', 'key', 'A', () => {
                keyTrap = key => {
                    settingsItems.Controls.Items[3][2] = assoc[key];
                    control.left = key
                };
                settingsItems.Controls.Items[3][2] = '..';
            }],
            ['Shot', 'key', 'Space', () => {
                keyTrap = key => {
                    settingsItems.Controls.Items[4][2] = assoc[key];
                    control.shot = key
                };
                settingsItems.Controls.Items[4][2] = '..';
            }],
            ['Shot 2', 'key', 'Shift', () => {
                keyTrap = key => {
                    settingsItems.Controls.Items[5][2] = assoc[key];
                    control.shot2 = key
                };
                settingsItems.Controls.Items[5][2] = '..';
            }],
        ]
    },
    'Volume': {
        'name': 'Volume',
        'img': 'sound',
        'Items': [
            ['Total volume', 'bar', [0, 10, 0], val => {
                settingsItems.Volume.Items[0][2][2] = val;
                volume.total = (val * 0.1)
            }],
            ['Musik', 'bar', [0, 10, 10], val => {
                settingsItems.Volume.Items[1][2][2] = val;
                volume.musik = (val * 0.1)
            }],
            ['Shots', 'bar', [0, 10, 10], val => {
                settingsItems.Volume.Items[2][2][2] = val;
                volume.shots = (val * 0.1)
            }],
            ['Hits', 'bar', [0, 10, 10], val => {
                settingsItems.Volume.Items[3][2][2] = val;
                volume.hits = (val * 0.1)
            }],
            ['Explosions', 'bar', [0, 10, 10], val => {
                settingsItems.Volume.Items[4][2][2] = val;
                volume.explosions = (val * 0.1)
            }],
        ]
    },
    'Close': {
        'name': 'Exit',
        'img': 'exit',
        'Items': [['Close', 'button', 'Close', Emenu],]
    }
};

const control = {
    forward: 87,
    backward: 83,
    turbo: 17,
    left: 65,
    right: 68,
    shot: 32,
    shot2: 16,
};

var particles = [];
var deads = [];
var plasma = [];

function CreateObject() {
    this.x = 0;
    this.y = 0;
    this.d = 0;
    this.sx = 0;
    this.sy = 0;
    this.sd = 0;
    this.controller = null;
    this.name = "no name";
    this.type = "block";
}

let objects =
    [
    {
        'controls': {},
        'controller': 'turel',
        'team': 'yellow',
        'name': 'Box',
        'type': 'block',
        'x': 5000,
        'y': 500,
        'd': 13,
        'sx': 0,
        'sy': 0,
        'sd': 0,
        'max': 50,
        'health': [10, 10],
        'model': 'Block'
    },
];

var stars = [];

function createObject(x, y, d, sx, sy, sd, type, model) {
    var helj = {};
    helj.x = x;
    helj.y = y;
    helj.d = d;
    helj.sx = sx;
    helj.sy = sy;
    helj.sd = sd;
    helj.type = type;
    helj.model = model;
    objects.push(helj);
}

var Sounds = {
    'shot:a': 'Audio/Fire/shot.wav',
    'hit': 'Audio/Fire/hit.wav',
    'explosions': '',
};

function fire(x, y, d, s, t, w, starter) {
    var helj = {};
    helj.x = x;
    helj.y = y;
    helj.d = d;
    helj.s = s;
    helj.t = t;
    helj.w = w;
    helj.starter = starter;
    helj.sound = new Audio();

    if (w) {
        var vol = Math.min((5000 / Long(animator.canvas.map.camera.x + (animator.canvas.map.camera.w / 2), animator.canvas.map.camera.y + (animator.canvas.map.camera.h / 2), x, y)) / 500, 1) * volume.total * volume.shots;
        if (vol > 0.003) {
            helj.sound = new Audio();
            helj.sound.volume = vol;
            helj.sound.src = Sounds['shot:' + t];
        }
    } else {
        var vol = Math.min((5000 / Long(animator.canvas.map.camera.x + (animator.canvas.map.camera.w / 2), animator.canvas.map.camera.y + (animator.canvas.map.camera.h / 2), x, y)) / 500, 1) * volume.total * volume.hits;
        if (vol > 0.003) {
            helj.sound.volume = vol;
            helj.sound.src = Sounds.hit;
        }
    }

    plasma.push(helj);
    plasma[plasma.length - 1].sound.play();
}

function start() {
    animator.add('map', 'ctx');
    animator.canvas.map.style.height = window.screen.height / 2;
    animator.canvas.map.style.width = window.screen.width / 2;
    animator.canvas.map.height = window.screen.height;
    animator.canvas.map.width = window.screen.width;
    animator.canvas.map.style.border = '1px solid black';

    animator.canvas.map.camera = {'x': 0, 'y': 0, 'z': 1, 'w': 100, 'h': 100};
    animator.canvas.map.camera.w = window.screen.width * 4;
    animator.canvas.map.camera.h = window.screen.height * 4;
    animator.canvas.map.camera.image = draw;
    animator.canvas.map.camera.x = objects[0].x - (window.screen.width / 2);
    animator.canvas.map.camera.y = objects[0].y - (window.screen.height / 2);

    animator.addImage('eye', 'img/Options/Video.png');
    animator.addImage('settings', 'img/Options/Settings.png');
    animator.addImage('full', 'img/Options/FullScreen.png');
    animator.addImage('control', 'img/Options/Controls.png');
    animator.addImage('sound', 'img/Options/Musik.png');
    animator.addImage('exit', 'img/Options/Exit.png');
    animator.addImage('menu', 'img/Options/Menu.png');
    animator.addImage('game', 'img/Options/Play.png');
    animator.addImage('turbo', 'img/Options/Turbo.png');
    animator.addImage('play', 'img/Musik/Play.png');
    animator.addImage('pause', 'img/Musik/Pause.png');
    animator.addImage('next', 'img/Musik/Next.png');
    animator.addImage('pre', 'img/Musik/Pre.png');
    animator.addImage('skull', 'img/GameContent/Dead.png');
    animator.addImage('rouret', 'img/Options/Disconnect.png');

    animator.start();
    // generationObjects();
    createStars();
    // PhisikRun();
    connector();
}

function draw(camera, vcan) {
    let j;
    let n;
    let i;
    const ctx = vcan.getContext('2d');

    /* calc */

    for (n = 0; n < particles.length; n++) {
        for (i = 0; i < objects.length; i++)
            if (
                Long(
                    particles[n].x + particles[n].sx,
                    particles[n].y + particles[n].sy,
                    objects[i].x,
                    objects[i].y
                ) < objects[i].max + 10) {
                for (j = 1; j < models[objects[i].model].sketh.length; j += 2)
                    if (
                        Intersection(
                            particles[n].x,
                            particles[n].y,
                            particles[n].x + particles[n].sx,
                            particles[n].y + particles[n].sy,
                            (Math.cos((models[objects[i].model].sketh[j - 1][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j - 1][1]) + objects[i].x + objects[i].sx,
                            (Math.sin((models[objects[i].model].sketh[j - 1][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j - 1][1]) + objects[i].y + objects[i].sy,
                            (Math.cos((models[objects[i].model].sketh[j][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j][1]) + objects[i].x + objects[i].sx,
                            (Math.sin((models[objects[i].model].sketh[j][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j][1]) + objects[i].y + objects[i].sy)
                    ) {
                        let spd = Math.sqrt(Math.pow(objects[i].sx - particles[n].sx, 2) + Math.pow(objects[i].sy - particles[n].sy, 2));
                        let barrier_dir = Diraction(
                            (Math.cos((models[objects[i].model].sketh[j - 1][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j - 1][1]) + objects[i].x,
                            (Math.sin((models[objects[i].model].sketh[j - 1][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j - 1][1]) + objects[i].y,
                            (Math.cos((models[objects[i].model].sketh[j][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j][1]) + objects[i].x,
                            (Math.sin((models[objects[i].model].sketh[j][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j][1]) + objects[i].y
                        );
                        let par_dir = Diraction(
                            particles[n].x,
                            particles[n].y,
                            particles[n].x + particles[n].sx,
                            particles[n].y + particles[n].sy
                        );
                        let newdir = barrier_dir + (barrier_dir - par_dir);

                        particles[n].sx = Math.cos(newdir) * spd;
                        particles[n].sy = Math.sin(newdir) * spd;
                        break;
                    }
            }

        particles[n].x += particles[n].sx;
        particles[n].y += particles[n].sy;

        particles[n].sx -= particles[n].sx * particlesLife;
        particles[n].sy -= particles[n].sy * particlesLife;

        particles[n].size = particles[n].sz / (particles[n].ss / (Math.abs(particles[n].sx) + Math.abs(particles[n].sy)));
    }

    for (n = 0; n < particles.length; n++)
        if ((Math.abs(particles[n].sx) + Math.abs(particles[n].sy) < 0.5) || (!StarShow(particles[n].x, particles[n].y)))
            particles.splice(n, 1);


    /*  */

    pilotObject = num;
    camera.x = objects[pilotObject].x - (camera.w / 2);
    camera.y = objects[pilotObject].y - (camera.h / 2);

    ctx.fillStyle = 'black';
    ctx.fillRect(0, 0, vcan.width, vcan.height);


    const DCX = SAVE_CAMERA_X - animator.canvas.map.camera.x;
    const DCY = SAVE_CAMERA_Y - animator.canvas.map.camera.y;
    SAVE_CAMERA_X = animator.canvas.map.camera.x;
    SAVE_CAMERA_Y = animator.canvas.map.camera.y;
    for (let n = 0; n < stars.length; ++n) {

        stars[n].x -= DCX / stars[n].z;
        stars[n].y -= DCY / stars[n].z;

        // if((stars[n].x - animator.canvas.map.camera.x - 10) > animator.canvas.map.camera.w)
        // 	stars[n].x -= animator.canvas.map.camera.w+objects[pilotObject].speed;
        // if((stars[n].x - animator.canvas.map.camera.x + 10) < 0)
        // 	stars[n].x += animator.canvas.map.camera.w+objects[pilotObject].speed;
        // if((stars[n].y - animator.canvas.map.camera.y - 10) > animator.canvas.map.camera.h)
        // 	stars[n].y -= animator.canvas.map.camera.h+objects[pilotObject].speed;
        // if((stars[n].y - animator.canvas.map.camera.y + 10) < 0)
        // 	stars[n].y += animator.canvas.map.camera.h+objects[pilotObject].speed;

        stars[n].x = animator.canvas.map.camera.x + loop(stars[n].x - animator.canvas.map.camera.x, 0, animator.canvas.map.camera.w);
        stars[n].y = animator.canvas.map.camera.y + loop(stars[n].y - animator.canvas.map.camera.y, 0, animator.canvas.map.camera.h);

        stars[n].v = StarShow(stars[n].x, stars[n].y);

    }

    for (n = 0; n < stars.length; n++) // Звёзды
        if (stars[n].v) {
            ctx.fillStyle = 'white';
            ctx.Star(stars[n].x, stars[n].y);
        }

    for (n = 0; n < particles.length; n++) { // Частицы
        ctx.fillStyle = particles[n].color;
        ctx.FRect(particles[n].x - (particles[n].size / 2), particles[n].y - (particles[n].size / 2), particles[n].size, particles[n].size);
    }

    ctx.strokeStyle = 'white';

    for (n = 0; n < objects.length; ++n) // Объекты
    {

        if (objects[n].controls.forward && objects[n].resurs.fuel[0] > 20) {

            const isTurbo = Boolean(objects[n].controls.turbo);
            const speed = isTurbo ? objects[n].speed * 3 : objects[n].speed;

            const particlesCount = isTurbo ? 50 : 5;
            const spread = isTurbo ? 2000 : 200;

            for (i = 0; i < particlesCount; ++i) {
                const fakeDir = rand(-spread, spread) / 100;

                const sx = (Math.cos((models[objects[n].model].bracings[0][2] + objects[n].d + fakeDir) * 0.0175) * speed);
                const sy = (Math.sin((models[objects[n].model].bracings[0][2] + objects[n].d + fakeDir) * 0.0175) * speed);

                particles.push({
                    x: objects[n].x + (Math.cos((models[objects[n].model].bracings[0][0] + objects[n].d) * 0.0175) * models[objects[n].model].bracings[0][1]) + rand(-speed, speed),
                    y: objects[n].y + (Math.sin((models[objects[n].model].bracings[0][0] + objects[n].d) * 0.0175) * models[objects[n].model].bracings[0][1]) + rand(-speed, speed),
                    sx,
                    sy,
                    sz: rand(6, 10),
                    size: 10,
                    color: 'rgb(' + rand(230, 255) + ',' + rand(140, 160) + ',0)',
                    ss: Math.abs(sx) + Math.abs(sy),
                })
            }

            for (i = 0; i < particlesCount; ++i) {
                const fakeDir = rand(-spread, spread) / 100;
                const sx = (Math.cos((models[objects[n].model].bracings[1][2] + objects[n].d + fakeDir) * 0.0175) * speed);
                const sy = (Math.sin((models[objects[n].model].bracings[1][2] + objects[n].d + fakeDir) * 0.0175) * speed);

                particles.push({
                    x: objects[n].x + (Math.cos((models[objects[n].model].bracings[1][0] + objects[n].d) * 0.0175) * models[objects[n].model].bracings[1][1]) + rand(-speed, speed),
                    y: objects[n].y + (Math.sin((models[objects[n].model].bracings[1][0] + objects[n].d) * 0.0175) * models[objects[n].model].bracings[1][1]) + rand(-speed, speed),
                    sx,
                    sy,
                    sz: rand(6, 10),
                    size: 10,
                    color: 'rgb(' + rand(230, 255) + ',' + rand(140, 160) + ',0)',
                    ss: Math.abs(sx) + Math.abs(sy),
                });
            }
        }

        if (objects[n].controls.backward && objects[n].resurs.fuel[0] > 10) {

            for (i = 0; i < 5; ++i) {
                const fakeDir = rand(-200, 200) / 100;
                const sx = (models[objects[n].model].bracings[2][2] + Math.cos((objects[n].d + fakeDir) * 0.0175) * rand(0, 15));
                const sy = (models[objects[n].model].bracings[2][2] + Math.sin((objects[n].d + fakeDir) * 0.0175) * rand(0, 15));

                particles.push({
                    x: objects[n].x + (Math.cos((models[objects[n].model].bracings[2][0] + objects[n].d) * 0.0175) * models[objects[n].model].bracings[2][1]) + rand(-5, 5),
                    y: objects[n].y + (Math.sin((models[objects[n].model].bracings[2][0] + objects[n].d) * 0.0175) * models[objects[n].model].bracings[2][1]) + rand(-5, 5),
                    sx,
                    sy,
                    sz: rand(6, 10),
                    size: 10,
                    color: 'rgb(0,0,' + rand(140, 160) +')',
                    ss: Math.abs(sx) + Math.abs(sy),
                });
            }

            for (i = 0; i < 5; ++i) {
                const fakeDir = rand(-200, 200) / 100;
                const sx = (models[objects[n].model].bracings[3][2] + Math.cos((objects[n].d + fakeDir) * 0.0175) * rand(0, 15));
                const sy = (models[objects[n].model].bracings[3][2] + Math.sin((objects[n].d + fakeDir) * 0.0175) * rand(0, 15));

                particles.push({
                    x: objects[n].x + (Math.cos((models[objects[n].model].bracings[3][0] + objects[n].d) * 0.0175) * models[objects[n].model].bracings[3][1]) + rand(-5, 5),
                    y: objects[n].y + (Math.sin((models[objects[n].model].bracings[3][0] + objects[n].d) * 0.0175) * models[objects[n].model].bracings[3][1]) + rand(-5, 5),
                    sx,
                    sy,
                    sz: rand(6, 10),
                    size: 10,
                    color: 'rgb(0,0,' + rand(140, 160) +')',
                    ss: Math.abs(sx) + Math.abs(sy)
                });
            }
        }

        ctx.beginPath();
        ctx.strokeStyle = 'white';
        if (models[objects[n].model] !== undefined)
            for (j = 1; j < models[objects[n].model].sketh.length; j += 2) {
                ctx.MT((Math.cos((models[objects[n].model].sketh[j - 1][0] + objects[n].d) * 0.0175) * models[objects[n].model].sketh[j - 1][1]) + objects[n].x, (Math.sin((models[objects[n].model].sketh[j - 1][0] + objects[n].d) * 0.0175) * models[objects[n].model].sketh[j - 1][1]) + objects[n].y);
                ctx.LT((Math.cos((models[objects[n].model].sketh[j][0] + objects[n].d) * 0.0175) * models[objects[n].model].sketh[j][1]) + objects[n].x, (Math.sin((models[objects[n].model].sketh[j][0] + objects[n].d) * 0.0175) * models[objects[n].model].sketh[j][1]) + objects[n].y);
            }
        ctx.stroke();

        if (models[objects[n].model].decor !== undefined) // Декоративные линии
        {
            ctx.beginPath();
            ctx.strokeStyle = 'white';
            for (j = 1; j < models[objects[n].model].decor.length; j += 2) {
                ctx.MT((Math.cos((models[objects[n].model].decor[j - 1][0] + objects[n].d) * 0.0175) * models[objects[n].model].decor[j - 1][1]) + objects[n].x, (Math.sin((models[objects[n].model].decor[j - 1][0] + objects[n].d) * 0.0175) * models[objects[n].model].decor[j - 1][1]) + objects[n].y);
                ctx.LT((Math.cos((models[objects[n].model].decor[j][0] + objects[n].d) * 0.0175) * models[objects[n].model].decor[j][1]) + objects[n].x, (Math.sin((models[objects[n].model].decor[j][0] + objects[n].d) * 0.0175) * models[objects[n].model].decor[j][1]) + objects[n].y);
            }
            ctx.stroke();
        }

        let flag = false;

        if (objects[n].team !== undefined)
            if (models[objects[n].model].strap !== undefined) // Погоны
            {

                for (i = 0; i < models[objects[n].model].strap.length; i++) {
                    ctx.beginPath();
                    ctx.fillStyle = objects[n].team;
                    ctx.MT((Math.cos((models[objects[n].model].strap[i][0][0] + objects[n].d) * 0.0175) * models[objects[n].model].strap[i][0][1]) + objects[n].x, (Math.sin((models[objects[n].model].strap[i][0][0] + objects[n].d) * 0.0175) * models[objects[n].model].strap[i][0][1]) + objects[n].y);
                    for (j = 1; j < models[objects[n].model].strap[i].length; j += 2)
                        ctx.LT((Math.cos((models[objects[n].model].strap[i][j][0] + objects[n].d) * 0.0175) * models[objects[n].model].strap[i][j][1]) + objects[n].x, (Math.sin((models[objects[n].model].strap[i][j][0] + objects[n].d) * 0.0175) * models[objects[n].model].strap[i][j][1]) + objects[n].y);
                    ctx.closePath();
                    ctx.fill();
                }
            } else
                flag = true;

        ctx.font = "normal " + (MenuSize / 2) + "px " + MenuFont;
        var nameW = ctx.measureText(objects[n].name).width;
        var w = nameW + (flag * (MenuSize * 1.2));

        ctx.beginPath();

        //ctx.TitleLine(objects[n].x,objects[n].y+objects[n].max,w)

        ctx.MT(objects[n].x - (w / 2), objects[n].y + objects[n].max);
        ctx.LT(objects[n].x + (w / 2), objects[n].y + objects[n].max);
        ctx.closePath();
        ctx.stroke();


        ctx.fillStyle = 'white';
        ctx.textBaseline = 'top';
        ctx.Text(objects[n].name, objects[n].x - (nameW / 2), objects[n].y + objects[n].max);

        if (flag) // Альтернатива - флаг
        {
            ctx.beginPath();
            ctx.fillStyle = objects[n].team;
            ctx.MT(objects[n].x + (w / 2) - (MenuSize / 10), objects[n].y + objects[n].max - 2);
            ctx.LT(objects[n].x + (w / 2) - (MenuSize / 2), objects[n].y + objects[n].max - 2);
            ctx.LT(objects[n].x + (w / 2) - (MenuSize / 2), objects[n].y + objects[n].max + (MenuSize / 1.7));
            ctx.LT(objects[n].x + (w / 2) - (MenuSize / 3.33), objects[n].y + objects[n].max + (MenuSize / 3));
            ctx.LT(objects[n].x + (w / 2) - (MenuSize / 10), objects[n].y + objects[n].max + MenuSize / 1.7);
            ctx.closePath();
            ctx.fill();

            ctx.beginPath();
            ctx.fillStyle = objects[n].team;
            ctx.MT(objects[n].x - (w / 2) + (MenuSize / 10), objects[n].y + objects[n].max - 2);
            ctx.LT(objects[n].x - (w / 2) + (MenuSize / 2), objects[n].y + objects[n].max - 2);
            ctx.LT(objects[n].x - (w / 2) + (MenuSize / 2), objects[n].y + objects[n].max + (MenuSize / 1.7));
            ctx.LT(objects[n].x - (w / 2) + (MenuSize / 3.33), objects[n].y + objects[n].max + (MenuSize / 3));
            ctx.LT(objects[n].x - (w / 2) + (MenuSize / 10), objects[n].y + objects[n].max + (MenuSize / 1.7));
            ctx.closePath();
            ctx.fill();
        }


        /*

		if ((objects[n].team == undefined) || (objects[n].team == objects[pilotObject].team))
		{
		if (objects[n].health != undefined){
			var u = 0;
			for (var name in objects[n].health){
			ctx.progressBar(objects[n].x-(w/2),objects[n].y+(15*u)+objects[n].max,w,12,objects[n].health[name],'rgba('+Math.round(255-(objects[n].health[name]*2.55))+','+Math.round(objects[n].health[name]*2.55)+',0,0.3)')
			u++;
			}
		}
		}
		*/

        /*ctx.font = "normal 10px "+MenuFont;
	ctx.fillStyle = 'yellow';
	if (objects[n].target > 0)
	ctx.Text(objects[n].rervo,objects[n].x,objects[n].y);
	//ctx.Text('Target: '+objects[objects[n].target].name,objects[n].x,objects[n].y);
	*/

        /*ctx.beginPath();
	ctx.MT(objects[n].x,objects[n].y);
	ctx.LT(objects[n].x+(Math.cos((10+objects[n].d)*0.0175)*300),objects[n].y+(Math.sin((10+objects[n].d)*0.0175)*300));
	ctx.MT(objects[n].x,objects[n].y);
	ctx.LT(objects[n].x+(Math.cos((350)*0.0175)*300),objects[n].y+(Math.sin(350*0.0175)*300));
	ctx.stroke();*/


    }

    for (n = 0; n < plasma.length; n++) {
        ctx.beginPath();
        ctx.strokeStyle = 'rgb(255,' + Math.round(Math.max(Math.min(plasma[n].s * 3, 250), 0)) + ',' + Math.round(Math.max(Math.min(plasma[n].s * 2, 250), 0)) + ')';
        ctx.MT(plasma[n].x, plasma[n].y);
        ctx.LT((Math.cos(plasma[n].d * 0.0175) * plasma[n].s) + plasma[n].x, (Math.sin(plasma[n].d * 0.0175) * plasma[n].s) + plasma[n].y);
        ctx.stroke();
    }

    for (let n = 0; n < deads.length; n++) {
        ctx.beginPath();
        ctx.strokeStyle = 'white';
        ctx.MT((Math.cos((deads[n].d + 180) * 0.0175) * deads[n].s) + deads[n].x, (Math.sin((deads[n].d + 180) * 0.0175) * deads[n].s) + deads[n].y);
        ctx.LT((Math.cos((deads[n].d) * 0.0175) * deads[n].s) + deads[n].x, (Math.sin((deads[n].d + 0) * 0.0175) * deads[n].s) + deads[n].y);
        ctx.stroke();
    }

    /*for (var n = 0; n < plasma.length; n++){
	var dir = Diraction(objects[0].x,objects[0].y,plasma[n].x,plasma[n].y);
	ctx.beginPath();
		ctx.strokeStyle = 'lime';
			ctx.MT(objects[0].x, objects[0].y);
			ctx.LT((Math.cos(dir)*200)+objects[0].x, (Math.sin(dir)*200)+objects[0].y);
		ctx.stroke();
	}*/

    ctx.fillStyle = 'black';
    ctx.strokeStyle = 'white';

    ctx.strokeRect(Math.round(MenuSize * 0.2) + 0.5, Math.round(MenuSize * 0.2) + 0.5, Math.round(MenuSize * 1.6), Math.round(MenuSize * 1.6));
    ctx.fillRect(Math.round(MenuSize * 0.2) + 0.5, Math.round(MenuSize * 0.2) + 0.5, Math.round(MenuSize * 1.6), Math.round(MenuSize * 1.6));
    if (objects[pilotObject].resurs !== undefined) {
        if (objects[pilotObject].resurs.fuel !== undefined) {
            ctx.fillStyle = 'black';
            ctx.fillRect(MenuSize * 2, MenuSize * 0.3, MenuSize * (objects[pilotObject].resurs.fuel[1] / 20), MenuSize * 0.6);
            ctx.fillStyle = 'rgb(255,' + Math.round(Math.max(Math.min(objects[pilotObject].resurs.fuel[0], 25) * 10, 0)) + ',' + Math.round(Math.max(Math.min(objects[pilotObject].resurs.fuel[0], 25) * 10, 0)) + ')';
            progressBar(ctx, MenuSize * 2, MenuSize * 0.3, MenuSize * (objects[pilotObject].resurs.fuel[1] / 20), MenuSize * 0.6, (objects[pilotObject].resurs.fuel[0] / objects[pilotObject].resurs.fuel[1]) * 100);
        }
        if (objects[pilotObject].resurs.energy !== undefined) {
            ctx.fillStyle = 'black';
            ctx.fillRect(MenuSize * 2, MenuSize * 1.1, MenuSize * (objects[pilotObject].resurs.energy[1] / 20), MenuSize * 0.6);
            ctx.fillStyle = 'rgb(255,' + Math.round(Math.max(Math.min(objects[pilotObject].resurs.energy[0], 25) * 10, 0)) + ',' + Math.round(Math.max(Math.min(objects[pilotObject].resurs.energy[0], 25) * 10, 0)) + ')';
            progressBar(ctx, MenuSize * 2, MenuSize * 1.1, MenuSize * (objects[pilotObject].resurs.energy[1] / 20), MenuSize * 0.6, (objects[pilotObject].resurs.energy[0] / objects[pilotObject].resurs.energy[1]) * 100);
        }
        if (objects[pilotObject].health !== undefined) {
            ctx.fillStyle = 'black';
            ctx.fillRect(Math.round(MenuSize * 0.2) + 0.5, (MenuSize * 2) + 0.5, MenuSize * (objects[pilotObject].health[1] / 20), MenuSize * 0.6, (objects[pilotObject].health[0] / objects[pilotObject].health[1]) * 100);
            ctx.fillStyle = 'rgb(255,' + Math.round(Math.max(Math.min(objects[pilotObject].health[0], 25) * 10, 0)) + ',' + Math.round(Math.max(Math.min(objects[pilotObject].health[0], 25) * 10, 0)) + ')';
            progressBar(ctx, Math.round(MenuSize * 0.2) + 0.5, (MenuSize * 2) + 0.5, MenuSize * (objects[pilotObject].health[1] / 20), MenuSize * 0.6, (objects[pilotObject].health[0] / objects[pilotObject].health[1]) * 100);
        }
    }
    ctx.beginPath();
    ctx.strokeStyle = 'white';
    for (j = 1; j < models[objects[pilotObject].model].sketh.length; j += 2) {
        ctx.moveTo((Math.cos((models[objects[pilotObject].model].sketh[j - 1][0] + (Date.now() / 10)) * 0.0175) * models[objects[pilotObject].model].sketh[j - 1][1] / (objects[pilotObject].max / MenuSize * 1.5)) + MenuSize * 1, (Math.sin((models[objects[pilotObject].model].sketh[j - 1][0] + (Date.now() / 10)) * 0.0175) * models[objects[pilotObject].model].sketh[j - 1][1] / (objects[pilotObject].max / MenuSize * 1.5)) + MenuSize * 1);
        ctx.lineTo((Math.cos((models[objects[pilotObject].model].sketh[j][0] + (Date.now() / 10)) * 0.0175) * models[objects[pilotObject].model].sketh[j][1] / (objects[pilotObject].max / MenuSize * 1.5)) + MenuSize * 1, (Math.sin((models[objects[pilotObject].model].sketh[j][0] + (Date.now() / 10)) * 0.0175) * models[objects[pilotObject].model].sketh[j][1] / (objects[pilotObject].max / MenuSize * 1.5)) + MenuSize * 1);
    }
    ctx.stroke();
    if (models[objects[pilotObject].model].decor !== undefined) {
        ctx.beginPath();
        ctx.strokeStyle = 'white';
        for (j = 1; j < models[objects[pilotObject].model].decor.length; j += 2) {
            ctx.moveTo((Math.cos((models[objects[pilotObject].model].decor[j - 1][0] + (Date.now() / 10)) * 0.0175) * models[objects[pilotObject].model].decor[j - 1][1] / (objects[pilotObject].max / MenuSize * 1.5)) + MenuSize * 1, (Math.sin((models[objects[pilotObject].model].decor[j - 1][0] + (Date.now() / 10)) * 0.0175) * models[objects[pilotObject].model].decor[j - 1][1] / (objects[pilotObject].max / MenuSize * 1.5)) + MenuSize * 1);
            ctx.lineTo((Math.cos((models[objects[pilotObject].model].decor[j][0] + (Date.now() / 10)) * 0.0175) * models[objects[pilotObject].model].decor[j][1] / (objects[pilotObject].max / MenuSize * 1.5)) + MenuSize * 1, (Math.sin((models[objects[pilotObject].model].decor[j][0] + (Date.now() / 10)) * 0.0175) * models[objects[pilotObject].model].decor[j][1] / (objects[pilotObject].max / MenuSize * 1.5)) + MenuSize * 1);
        }
        ctx.stroke();
    }

    if (messages.length > 0) {
        var alph = 1;
        if ((Date.now() - messTime[0]) < messTime[1])
            alph = (Date.now() - messTime[0]) / messTime[1];
        if ((Date.now() - messTime[0]) > messTime[1] + messTime[2])
            alph = 1 - ((Date.now() - messTime[0] - messTime[1] - messTime[2]) / messTime[3]);
        if ((Date.now() - messTime[0]) > messTime[1] + messTime[2] + messTime[3])
            alph = 0;

        ctx.globalAlpha = alph;
        var x = (animator.canvas.map.width - messages[0][0].width) / 2
        var y = 200;
        ctx.drawImage(messages[0][0], x, y);

        if ((Date.now() - messTime[0]) > messTime[1] + messTime[2] + messTime[3]) {
            messages.splice(0, 1);
            messTime[0] = Date.now();
        }
        ctx.globalAlpha = 1;
    } else
        messTime[0] = Date.now();

    /* fps */
    ctx.font = "normal 20px " + MenuFont;
    vcan.style.fontSize = '100px';
    ctx.fillStyle = 'lime';
    ctx.strokeStyle = 'lime';
    ctx.textBaseline = 'top';
    ctx.CText('FPS: ' + Math.round(1000 / (Date.now() - fps)), 5, 75);
    fps = Date.now();
}

function progressBar(ctx, x, y, w, h, progress, color) {
    //ctx.fillStyle = color;
    //ctx.strokeStyle = color;
    ctx.strokeRect(Math.round(x) - 0.5, Math.round(y) - 0.5, Math.round(w), Math.round(h))
    ctx.fillRect(Math.round(x + 1) - 0.5, Math.round(y + 1) - 0.5, ((w * (progress / 100)) - 2), Math.round(h - 2))
}

function Message(img, txt, id) {
    var content = document.createElement('canvas');
    context = content.getContext('2d');
    context.font = 'normal ' + MenuSize + 'px ' + MenuFont;
    content.width = MenuSize + (MenuSize / 2) + context.measureText(txt).width;
    content.height = MenuSize;

    context.font = 'normal ' + MenuSize + 'px ' + MenuFont;
    context.fillStyle = 'white';
    context.strokeStyle = 'gray';
    context.textBaseline = 'top';
    context.fillText(txt, MenuSize * 1.5, 0);
    context.strokeText(txt, MenuSize * 1.5, 0);
    context.drawImage(animator[img], 0, 0, MenuSize, MenuSize);


    if ((messages.length > 0) && (id !== undefined) && (id === messages[0][1])) {
        messages[0][0] = content;
        messTime[0] = Date.now();
    } else {
        var helparr = [,];
        helparr[0] = content;
        helparr[1] = id;
        messages.push(helparr);
    }
}

function KillMessage(obj1, obj2) {
    var name1 = 'NoName'
    if (obj1.name != undefined)
        name1 = obj1.name;
    if (obj2.name != undefined) {
        var name2 = obj2.name;
        var content = document.createElement('canvas');
        context = content.getContext('2d');
        context.font = 'normal ' + MenuSize + 'px ' + MenuFont;
        content.width = context.measureText(name1 + ' to killed ' + name2).width;
        content.height = MenuSize;

        context.font = 'normal ' + MenuSize + 'px ' + MenuFont;
        context.textBaseline = 'top';
        var xx = 0;
        context.fillStyle = obj1.team;
        context.fillText(name1, xx, 0);
        xx += context.measureText(name1).width;
        context.fillStyle = 'white';
        context.fillText(' to killed ', xx, 0);
        xx += context.measureText(' to killed ').width;
        context.fillStyle = obj2.team;
        context.fillText(name2, xx, 0);

        var id = name1 + ' to killed ' + name2;

        var findId = false;

        for (var m = 0; m < messages.length; m++)
            if (messages[m][1] == id)
                findId = true;

        if (!findId) {
            var helparr = [,];
            helparr[0] = content;
            helparr[1] = name1 + ' to killed ' + name2;
            messages.push(helparr);
        }
    }
}

function PhisikRun() {
    // phisik();
}

function hit(object, line, hot, objectKiller) {
    var realHot = hot / 50;
    var realLine = (line - 1) / 2;
    if (objects[object].health != undefined) {
        objects[object].health[0] -= realHot;

        if (objects[object].health[0] < 0)
            dead(object, objectKiller);
    }
}

function generationObjects() {
    phisik(true);
    for (var i = 0; i < 20; i++) {
        var s = new CreateObject();
        var modelName = 'Meteor#' + rand(1000, 10000);
        var size = rand(20, 1000);
        var gran = rand(12, 20);
        var model = [];
        var jj = 0;
        var two = [0, 0];
        two[1] = rand(size - gran, size + gran)
        model.push(two);
        for (var j = 0; j < gran; j++) {
            two = [0, 0];
            jj += rand((360 / gran) - 5, 360 / gran);
            two[0] = jj;
            two[1] = rand(size - gran, size + gran)
            model.push(two);
            model.push(two);
        }
        model.push(model[0]);

        s.model = modelName;

        var md = {};
        md.sketh = model;
        models[modelName] = md;

        var max = 0;
        for (var j = 0; j < model.length; j++)
            max = Math.max(max, model[j][1]);
        s.max = max;
        s.name = modelName;
        s.type = 'aster';
        s.d = rand(0, 360);
        s.health = [1000, 1000]

        do {
            s.x = rand(-20000, 20000);
            s.y = rand(-20000, 20000);
        } while (circleConflict(s.x, s.y, max) >= 0);

        objects.push(s);
    }
}

function createStars() {
    stars.length = 0;
    for (var i = 0; i < starsMany; i++) {
        var s = {'x': 0, 'y': 0, 'z': 0, 'v': true,}

        s.x = rand(animator.canvas.map.camera.x - 10, animator.canvas.map.camera.x + animator.canvas.map.camera.w + 10);
        s.y = rand(animator.canvas.map.camera.y - 10, animator.canvas.map.camera.y + animator.canvas.map.camera.h + 10);
        s.z = rand(2, 5);

        stars.push(s);
    }
}

function Bmenu() {
    clearTimeout(phisikWhile);
    clearInterval(animator.canvas.map.run);

    animator.canvas.map.ctx.beginPath();
    for (var l = 0; l < animator.canvas.map.height; l++) {
        if ((l % 3) == 0)
            animator.canvas.map.ctx.strokeStyle = 'rgba(0,0,0,0.7)';
        else
            animator.canvas.map.ctx.strokeStyle = 'rgba(0,0,0,0.5)';
        animator.canvas.map.ctx.beginPath();
        animator.canvas.map.ctx.moveTo(0, l + 0.5);
        animator.canvas.map.ctx.lineTo(animator.canvas.map.width, l + 0.5);
        animator.canvas.map.ctx.stroke();
    }

    MenuBackground = document.createElement('canvas');
    MenuBackground.width = animator.canvas.map.width;
    MenuBackground.height = animator.canvas.map.height;
    MenuBackground.getContext('2d').drawImage(animator.canvas.map, 0, 0);

    MenuCloseId = setInterval(function () {
        drawMenu(animator.canvas.map.ctx)
    }, 100);
    MenuMusik = !player.paused;
    player.pause();
    paused = true;
}

function Emenu() {
    clearInterval(MenuCloseId);
    createStars();
    animator.start();
    PhisikRun();
    MenuBackground = undefined;
    player.volume = volume.musik;
    if (MenuMusik) player.play();
    paused = false;
}

function drawMenu(ctx) {
    ItemTab = Math.min(Math.max(ItemTab, 0), menuItems.length - 1);

    var top = (animator.canvas.map.height - (menuItems.length * (MenuSize * 1.1))) / 2;
    var left = 0;

    for (var i = 0; i < menuItems.length; i++)
        left = Math.max(left, ctx.measureText(menuItems[i][1]).width);
    left = (animator.canvas.map.width - left) / 2;


    ctx.font = 'normal ' + MenuSize + 'px ' + MenuFont;
    ctx.textBaseline = 'top';
    ctx.drawImage(MenuBackground, 0, 0, animator.canvas.map.width, animator.canvas.map.height);
    for (var i = 0; i < menuItems.length; i++) {
        ctx.Image(menuItems[i][0], left - (MenuSize * 1.1), (i * (MenuSize * 1.1)) + top, MenuSize, MenuSize);
        if (i == ItemTab) {
            ItemScript = menuItems[i][2];
            ctx.fillStyle = 'lime';
            ctx.strokeStyle = 'lime';
        } else {
            ctx.fillStyle = 'white';
            ctx.strokeStyle = 'white';
        }
        ctx.CText(menuItems[i][1], left, (i * (MenuSize * 1.1)) + top);
    }
}

var tp = 10;

function Settings(ctx) {
    var n = 0;

    var width = 0;
    var left = MenuSize * 1.3;
    var bottom = 0;


    for (var set in settingsItems)
        for (var j = 0; j < settingsItems[set].Items.length; j++) {
            n++;
            width = Math.max(width, ctx.measureText(settingsItems[set].Items[j][0]).width);
        }

    ItemTab = Math.max(Math.min(ItemTab, n - 1), 0)
    left = (animator.canvas.map.width - width) / 2 - MenuSize * 1.3;

    n = 0;
    var i = 0;
    var j = 0;


    ctx.drawImage(MenuBackground, 0, 0, animator.canvas.map.width, animator.canvas.map.height);
    ctx.font = 'normal ' + MenuSize + 'px ' + MenuFont;
    ctx.textBaseline = 'top';

    for (var set in settingsItems) {
        ctx.fillStyle = '#5555FF';
        ctx.strokeStyle = '#5555FF';
        ctx.Image(settingsItems[set].img, left - (MenuSize * 1.1), (i * (MenuSize * 1.2)) + tp, MenuSize, MenuSize);
        ctx.CText(settingsItems[set].name, left, (i * (MenuSize * 1.2)) + tp);
        i++;
        for (var j = 0; j < settingsItems[set].Items.length; j++) {
            if (n == ItemTab) {
                ItemScript = settingsItems[set].Items[j][3];
                bottom = i * (MenuSize * 1.2);
                if (settingsItems[set].Items[j][1] == 'bar') {
                    ItemMin = settingsItems[set].Items[j][2][0];
                    ItemMax = settingsItems[set].Items[j][2][1];
                    ItemValue = settingsItems[set].Items[j][2][2];
                } else if (settingsItems[set].Items[j][1] == 'key') {
                    ItemValue = settingsItems[set].Items[j][2];
                } else if (settingsItems[set].Items[j][1] == 'pick') {
                    ItemMin = 0;
                    ItemMax = settingsItems[set].Items[j][2][0].length - 1;
                    if (typeof settingsItems[set].Items[j][2][1] == 'number')
                        ItemValue = settingsItems[set].Items[j][2][1];
                    else
                        ItemValue = 0;
                }
                ctx.fillStyle = 'lime';
                ctx.strokeStyle = 'lime';
            } else {
                ctx.fillStyle = 'white';
                ctx.strokeStyle = 'white';
            }
            ctx.CText(settingsItems[set].Items[j][0], left - (MenuSize * 0.5), (i * (MenuSize * 1.2)) + tp);
            if (settingsItems[set].Items[j][1] == 'bar')
                progressBar(ctx, left + width + 5, (i * (MenuSize * 1.2)) + tp + (MenuSize / 4), (MenuSize * 2.5), (MenuSize / 2), Map(settingsItems[set].Items[j][2][2], settingsItems[set].Items[j][2][0], settingsItems[set].Items[j][2][1], 0, 100))
            else if (settingsItems[set].Items[j][1] == 'key')
                ctx.CText('[' + settingsItems[set].Items[j][2] + ']', left + width + 5, (i * (MenuSize * 1.2)) + tp);
            else if (settingsItems[set].Items[j][1] == 'pick')
                if (typeof settingsItems[set].Items[j][2][1] == 'number')
                    ctx.CText('<' + settingsItems[set].Items[j][2][0][settingsItems[set].Items[j][2][1]] + '>', left + width + 5, (i * (MenuSize * 1.2)) + tp);
                else
                    ctx.CText('<' + settingsItems[set].Items[j][2][1] + '>', left + width + 5, (i * (MenuSize * 1.2)) + tp);
            n++;
            i++;
        }
    }

    if ((tp + bottom + (MenuSize * 1.2)) > animator.canvas.map.camera.h) tp -= (MenuSize / 4);
    if ((tp + bottom - (MenuSize * 1.2)) < 0) tp += (MenuSize / 4);

}

var Musik = {
    'start': function () {
        soundNo = 0;
        player.src = playList[soundNo][1];
        player.play();
        player.volume = volume.musik;
        Message('sound', playList[soundNo][0], 'Musik');
    },
    'pause': function () {
        player.pause();
        Message('pause', playList[soundNo][0], 'Musik');
    },
    'play': function () {
        player.play();
        Message('play', playList[soundNo][0], 'Musik');
    },
    'next': function () {
        if (soundNo < playList.length - 1) {
            soundNo++;
            player.src = playList[soundNo][1];
            player.play();
            player.volume = volume.musik;
            Message('next', playList[soundNo][0], 'Musik');
        } else
            Message('sound', playList[soundNo][0], 'Musik');
    },
    'pre': function () {
        if (soundNo > 0) {
            soundNo--;
            player.src = playList[soundNo][1];
            player.play();
            player.volume = volume.musik;
            Message('pre', playList[soundNo][0], 'Musik');
        } else
            Message('sound', playList[soundNo][0], 'Musik');
    },
};

function StarShow(x, y) {
    for (var i = 0; i < objects.length; i++)
        if (Long(objects[i].x, objects[i].y, x, y) < objects[i].max) {
            var dir = Diraction(objects[i].x, objects[i].y, x, y);
            var chet = 2;
            for (var j = 1; j < models[objects[i].model].sketh.length; j += 2)
                if (Intersection(x, y, x + (Math.cos(dir) * objects[i].max), y + (Math.sin(dir) * objects[i].max), (Math.cos((models[objects[i].model].sketh[j - 1][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j - 1][1]) + objects[i].x, (Math.sin((models[objects[i].model].sketh[j - 1][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j - 1][1]) + objects[i].y, (Math.cos((models[objects[i].model].sketh[j][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j][1]) + objects[i].x, (Math.sin((models[objects[i].model].sketh[j][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j][1]) + objects[i].y))
                    chet++;
            if ((chet % 2) != 0)
                return false;
        }
    return true;
}

function circleConflict(x, y, r) {
    for (var i = 0; i < objects.length; i++) {
        if (Long(objects[i].x, objects[i].y, x, y) < objects[i].max + r)
            return i;
    }
    return -10;
}

function Intersection(ax1, ay1, ax2, ay2, bx1, by1, bx2, by2) {
    var v1, v2, v3, v4;
    v1 = (bx2 - bx1) * (ay1 - by1) - (by2 - by1) * (ax1 - bx1);
    v2 = (bx2 - bx1) * (ay2 - by1) - (by2 - by1) * (ax2 - bx1);
    v3 = (ax2 - ax1) * (by1 - ay1) - (ay2 - ay1) * (bx1 - ax1);
    v4 = (ax2 - ax1) * (by2 - ay1) - (ay2 - ay1) * (bx2 - ax1);
    return (v1 * v2 < 0) && (v3 * v4 < 0);
}

function Diraction(x1, y1, x2, y2) {
    if (x2 > x1)
        return Math.atan((y2 - y1) / (x2 - x1));
    else
        return Math.PI + Math.atan((y2 - y1) / (x2 - x1));
}

function Scalar(x1, y1, x2, y2) {
    return (x1 * x2) + (y1 * y2)
}

function Long(x1, y1, x2, y2) {
    return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
}

function ddir(d0, d1) {
    return Math.asin(Math.sin(d0) - Math.sin(d1));
}

const loop = (val, min, max) => {
    let len = max - min;
    val = (val - min) % len;
    if (val < 0)
        val += len;
    return val + min;
};