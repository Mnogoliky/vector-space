const keys = {};

let keyTrap = undefined;

const assoc = {
	'114': 'F3',
	'16': 'Shift',
	'32': 'Space',
	'33': 'PgUp',
	'34': 'PgDn',
	'37': 'ArrowLeft',
	'38': 'ArrowTop',
	'39': 'ArrowRight',
	'40': 'ArrowDown',
	'65': 'A',
	'68': 'D',
	'69': 'E',
	'81': 'Q',
	'83': 'S',
	'87': 'W',
	'80': 'P',
};

document.onkeydown = function (event) {
	keys[event.keyCode] = true;
	if (keyTrap !== undefined) {
		keyTrap(event.keyCode);
		keyTrap = undefined;
	}
	else {
		if (Hotkey[event.keyCode] !== undefined)
			Hotkey[event.keyCode]();
		if (assoc[event.keyCode] !== undefined)
			keys[assoc[event.keyCode]] = true;
	}
	return false;
};
document.onkeyup = function (event) {
	keys[event.keyCode] = false;
	if (assoc[event.keyCode] !== undefined)
		keys[assoc[event.keyCode]] = false;
	return false;
};

var Hotkey = {
	115: () => {
		connector();
	},
	'114': function () {
		getMap(); //Message('eye', 'XUY');
	},
	'190': function () {
		Musik.next();
	},
	'188': function () {
		Musik.pre();
	},
	'113': function () {
		Mirrow = true
	},//console.log(JSON.stringify(objects[0]));},
	'123': function () {
		Full('map');
	},
	'116': location.reload,
	'9': function () {
		if (paused) Emenu(); else Bmenu();
		ItemScript = undefined;
	},
	'38': function () {
		if (paused) {
			ItemTab--;
			ItemPick.play();
		}
	},
	'40': function () {
		if (paused) {
			ItemTab++;
			ItemPick.play();
		}
	},
	'13': function () {
		if (paused) {
			ItemClick.play();
		}
		if (ItemScript !== undefined) ItemScript(ItemValue);
		ItemScript = undefined;
	},
	'37': function () {
		if (paused) {
			ItemClick.play();
		}
		if (ItemScript !== undefined) if (ItemMin > ItemMax) ItemScript(Math.min(Math.max(ItemValue + 1, ItemMax), ItemMin)); else ItemScript(Math.max(Math.min(ItemValue - 1, ItemMax), ItemMin));
		ItemScript = undefined;
	},
	'39': function () {
		if (paused) {
			ItemClick.play();
		}
		if (ItemScript !== undefined) if (ItemMin > ItemMax) ItemScript(Math.min(Math.max(ItemValue - 1, ItemMax), ItemMin)); else ItemScript(Math.max(Math.min(ItemValue + 1, ItemMax), ItemMin));
		ItemScript = undefined;
	},
};