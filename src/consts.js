
/*
Constants


*/

export const needPlayers = 1;

export const quality = 10;

export const phisikHZ = 1000/25;

export const rocketModels = [
    'Fly',
    // 'Star Jumper',
    // 'Arrow',
    'Pilgrim',
]

export const teams =
    [
        'red',
        'purple',
        'lime',
        'yellow',
    ];

export const port = process.env.APP_PORT || 9999;