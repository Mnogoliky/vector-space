import {objects} from "./objects";
import {models} from "./Models";

export const circleConflict = (x, y, r) => {
    for (let i = 0; i < objects.length; i++) {
        if (getDistance(objects[i].x, objects[i].y, x, y) < objects[i].max + r)
            return i;
    }
    return -10;
};

export const lineIntersection = (ax1, ay1, ax2, ay2, bx1, by1, bx2, by2) => {
    let v1, v2, v3, v4;
    v1 = (bx2 - bx1) * (ay1 - by1) - (by2 - by1) * (ax1 - bx1);
    v2 = (bx2 - bx1) * (ay2 - by1) - (by2 - by1) * (ax2 - bx1);
    v3 = (ax2 - ax1) * (by1 - ay1) - (ay2 - ay1) * (bx1 - ax1);
    v4 = (ax2 - ax1) * (by2 - ay1) - (ay2 - ay1) * (bx2 - ax1);
    return (v1 * v2 < 0) && (v3 * v4 < 0);
};

export const getDirection = (x1, y1, x2, y2) => {
    if (x2 > x1)
        return Math.atan((y2 - y1) / (x2 - x1));
    else
        return Math.PI + Math.atan((y2 - y1) / (x2 - x1));
};

export const getIsNotUnderObject = (x, y) => {
    for (let i = 0; i < objects.length; i++)
        if (getDistance(objects[i].x, objects[i].y, x, y) < objects[i].max) {
            let dir = getDirection(objects[i].x, objects[i].y, x, y);
            let chet = 2;
            for (let j = 1; j < models[objects[i].model].sketh.length; j += 2) {
                if (lineIntersection(
                    x,
                    y,
                    x + (Math.cos(dir) * objects[i].max), y + (Math.sin(dir) * objects[i].max),
                    (Math.cos((models[objects[i].model].sketh[j - 1][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j - 1][1]) + objects[i].x, (Math.sin((models[objects[i].model].sketh[j - 1][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j - 1][1]) + objects[i].y, (Math.cos((models[objects[i].model].sketh[j][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j][1]) + objects[i].x, (Math.sin((models[objects[i].model].sketh[j][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j][1]) + objects[i].y
                )) {
                    ++chet;
                }
            }
            if ((chet % 2) !== 0) {
                return false;
            }
        }
    return true;
};


export const Scalar = (x1, y1, x2, y2) => (x1 * x2) + (y1 * y2);

export const getDistance = (x1, y1, x2, y2) => Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));

export const map = (inn, min1, max1, min2, max2) => (((inn - min1) / (max1 - min1)) * (max2 - min2)) + min2;
