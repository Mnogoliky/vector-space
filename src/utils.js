export const getDataFromUrl = url => {
	return url.split('?')
			.pop()
			.split('&')
			.reduce((accum, str) => {
				const [key, value] = str.split('=');
				return {...accum, [key]: value};
			}, {})
};

export const breadCrumbs = url => {
	const rez = url.split('?').shift().split('/');
	rez.shift();
	return rez;
};

export const rand = (min, max) => {
	return Math.round(Math.random() * (max - min) + min);
};

export const loop = (val, min, max) => {
	let len = max - min;
	val = (val-min) % len;
	if (val < 0)
		val += len;
	return val+min;
};