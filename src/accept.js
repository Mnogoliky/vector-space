import express from 'express'
import path from 'path';
import { connectPlayer } from "./connector";
import { models } from "./Models";
import expressApp from "./index";

/* variables */


/* script */

expressApp.use('/js/', express.static(path.resolve(__dirname, '..', 'js')));
expressApp.use('/css/', express.static(path.resolve(__dirname, '..', 'css')));
expressApp.use('/img/', express.static(path.resolve(__dirname, '..', 'img')));
expressApp.use('/', express.static(path.resolve(__dirname, '..')));
expressApp.use('/favicon.ico', express.static(path.resolve(__dirname, '..', 'favicon.ico')));

expressApp.get('/api/connect', (req, res) => {
	res.writeHead(200);
	res.end(JSON.stringify(connectPlayer(req.query)));
})

expressApp.get('/api/restart', (req, res) => {
	res.writeHead(200);
	res.end({ok: true});

	setTimeout(() => process.exit(0), 1000);
})

expressApp.get('/api/models', (req, res) => {
	res.writeHead(200);
	res.end(JSON.stringify(models));
})