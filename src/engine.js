import {objects, findFreeRocket} from "./objects";
import {models} from "./Models";
import {rand} from "./utils";
import {plasma, fire} from "./plasma";
import {getDistance, getDirection, lineIntersection, Scalar, map, getIsNotUnderObject} from "./Geometry";
import {quality, phisikHZ} from "./consts";
import {assoc} from "./connector";
import {sendMap} from "./ws";

export let started = false;

export const startEngine = () => {
    console.log('\x1b[36m%s\x1b[0m', 'Starting engine');
    phisik();
    started = true;
};

const saveTorrelleData = new WeakMap();

const objectPhisik = {
    rocket(n) {
        if (objects[n].controls.forward) {
            if (objects[n].controls.turbo) {
                if (objects[n].resurs.fuel[0] > 10) {
                    objects[n].sx += (Math.cos(objects[n].d * 0.0175) * objects[n].speed * 3) * (10 / objects[n].mass);
                    objects[n].sy += (Math.sin(objects[n].d * 0.0175) * objects[n].speed * 3) * (10 / objects[n].mass);
                    objects[n].resurs.fuel[0] -= 0.2;
                }
            } else {
                if (objects[n].resurs.fuel[0] > 1) {
                    objects[n].sx += (Math.cos(objects[n].d * 0.0175) * objects[n].speed) * (10 / objects[n].mass);
                    objects[n].sy += (Math.sin(objects[n].d * 0.0175) * objects[n].speed) * (10 / objects[n].mass);
                    objects[n].resurs.fuel[0] -= 0.02;
                }
            }
        }
        if (objects[n].controls.backward) {
            if (objects[n].resurs.fuel[0] > 1) {
                objects[n].sx += (Math.cos((180 + objects[n].d) * 0.0175) * objects[n].speed) * (5 / objects[n].mass);
                objects[n].sy += (Math.sin((180 + objects[n].d) * 0.0175) * objects[n].speed) * (5 / objects[n].mass);
                objects[n].resurs.fuel[0] -= 0.01;
            }
        }

        if (objects[n].controls.fire) {
            if (objects[n].fsave === undefined) {
                objects[n].fsave = Date.now() - objects[n].ftime;
                objects[n].fn = true;
            }
            if (Date.now() - objects[n].fsave > objects[n].ftime) {
                objects[n].fn = !objects[n].fn;
                for (let ss = 0; ss < 1; ss++)
                    if (objects[n].resurs.energy[0] > 0.1) {
                        const fakeDir = rand(-30, 30) / 20;
                        const spd = Math.sqrt(Math.pow(objects[n].sx, 2) + Math.pow(objects[n].sy, 2));//(objects[n].speed*2)
                        fire(objects[n].x + Math.cos((objects[n].d + models[objects[n].model].bracings[4 + objects[n].fn][0]) * 0.0175) * (models[objects[n].model].bracings[4 + objects[n].fn][1] + spd), objects[n].y + Math.sin((objects[n].d + models[objects[n].model].bracings[4 + objects[n].fn][0]) * 0.0175) * (models[objects[n].model].bracings[4 + objects[n].fn][1] + spd), (objects[n].d + fakeDir), 500, 'a', true, objects[n]);

                        objects[n].resurs.energy[0] -= 0.1;
                    }

                objects[n].fsave = Date.now();
            }
        }
        if (objects[n].controls.fire2) {
            if (objects[n].fsave2 === undefined) {
                objects[n].fsave2 = Date.now() - objects[n].ftime2;
                objects[n].fn2 = true;
            }
            if (Date.now() - objects[n].fsave2 >= objects[n].ftime2) {
                objects[n].fn2 = !objects[n].fn2;
                for (let ss = 0; ss < 7; ss++)
                    if (objects[n].resurs.energy[0] > 0.3) {
                        const fakeDir = rand(-500, 500) / 200;
                        const spd = Math.sqrt(Math.pow(objects[n].sx, 2) + Math.pow(objects[n].sy, 2));//(objects[n].speed*2)
                        fire(
                            objects[n].x + Math.cos((objects[n].d + models[objects[n].model].bracings[4 + objects[n].fn2][0]) * 0.0175) * (models[objects[n].model].bracings[4 + objects[n].fn2][1] + spd),
                            objects[n].y + Math.sin((objects[n].d + models[objects[n].model].bracings[4 + objects[n].fn2][0]) * 0.0175) * (models[objects[n].model].bracings[4 + objects[n].fn2][1] + spd),
                            (objects[n].d + fakeDir),
                            800,
                            'a',
                            true,
                            objects[n]
                        );

                        objects[n].resurs.energy[0] -= 0.3;
                    }

                objects[n].fsave2 = Date.now();
            }
        }
        if (objects[n].controls.turnRight)
            if (objects[n].resurs.energy[0] > 1) {
                objects[n].sd += (10 - objects[n].sd) * (7 / objects[n].mass);
                objects[n].resurs.energy[0] -= 0.02;
            }

        if (objects[n].controls.turnLeft)
            if (objects[n].resurs.energy[0] > 1) {
                objects[n].sd -= (10 + objects[n].sd) * (7 / objects[n].mass);
                objects[n].resurs.energy[0] -= 0.02;
            }
    },
    turel(n) {
        if (objects[n].servo === undefined) objects[n].servo = 5;
        if (objects[n].rervo === undefined) objects[n].rervo = objects[n].servo;

        if (objects[n].controls.fire) {
            if (objects[n].fsave === undefined) {
                objects[n].fsave = Date.now() - objects[n].ftime;
                objects[n].fn = true;
            }
            if (Date.now() - objects[n].fsave > objects[n].ftime) {
                objects[n].fn = !objects[n].fn;
                for (let ss = 0; ss < 1; ss++)
                    if (objects[n].resurs.energy[0] > 0.1) {
                        let fakeDir = rand(-10, 10) / 20;
                        let spd = Math.sqrt(Math.pow(objects[n].sx, 2) + Math.pow(objects[n].sy, 2));//(objects[n].speed*2)
                        fire(objects[n].x + Math.cos((objects[n].d + models[objects[n].model].bracings[4 + objects[n].fn][0]) * 0.0175) * (models[objects[n].model].bracings[4 + objects[n].fn][1] + spd), objects[n].y + Math.sin((objects[n].d + models[objects[n].model].bracings[4 + objects[n].fn][0]) * 0.0175) * (models[objects[n].model].bracings[4 + objects[n].fn][1] + spd), (objects[n].d + fakeDir), 250, 'a', true, objects[n]);

                        objects[n].resurs.energy[0] -= 0.1;
                    }

                objects[n].fsave = Date.now();
            }
        }
        if (objects[n].controls.turnRight)
            if (objects[n].resurs.energy[0] > 1) {
                objects[n].sd += (objects[n].rervo - objects[n].sd) * (7 / objects[n].mass);
                objects[n].resurs.energy[0] -= 0.02;
            }

        if (objects[n].controls.turnLeft)
            if (objects[n].resurs.energy[0] > 1) {
                objects[n].sd -= (objects[n].rervo + objects[n].sd) * (7 / objects[n].mass);
                objects[n].resurs.energy[0] -= 0.02;
            }
    },
};

const controller = obj => {
    if (obj.controller === 'turel') {

        if (obj.catchTargetTime === undefined) {
            obj.catchTargetTime = Date.now();
        }


        if (
            (Date.now() - obj.catchTargetTime > 12000) ||
            saveTorrelleData.get(obj) === undefined ||
            saveTorrelleData.get(obj) !== objects[obj.target]
        ) {
            obj.target = 0;
            let targetDistance = Infinity;

            for (let p = 0; p < objects.length; p++) {
                if ((objects[p].team !== undefined) && (objects[p].team !== 'white') && (objects[p].name !== obj.name)) {
                    if ((objects[p].team === 'gray') || (objects[p].team !== obj.team)) {

                        const currentDistance = getDistance(obj.x, obj.y, objects[p].x, objects[p].y);

                        if (currentDistance < targetDistance) {
                            obj.target = p;
                            targetDistance = currentDistance;
                        }
                    }
                }
            }

            obj.catchTargetTime = Date.now();
            saveTorrelleData.set(obj, objects[obj.target]);
        }


        if (obj.target !== undefined) {
            obj.controls.fire = controll_diraction(obj);
        } else {
            controll_nocommand(obj);
        }
    }
};

function controll_diraction(obj) {
    const dr = (obj.d + 90) * 0.0175; //Diraction(obj.x,obj.y,objects[obj.target].x,objects[obj.target].y);
    const lg = getDistance(obj.x, obj.y, objects[obj.target].x, objects[obj.target].y);
    const s = Scalar(obj.x - objects[obj.target].x, obj.y - objects[obj.target].y, Math.cos(dr) * lg, Math.sin(dr) * lg)
    const a = Math.abs(s) < 0;
    const min = Math.abs(s / (lg * lg)) * 57.3;
    obj.rervo = Math.min(obj.servo, (min) / 15);

    if (s < 0) {
        obj.controls.turnRight = true;
        obj.controls.turnLeft = false;
    } else {
        obj.controls.turnRight = false;
        obj.controls.turnLeft = true;
    }
    return (min < 1);
}

function controll_nocommand(obj) {
    obj.controls.turnRight = false;
    obj.controls.turnLeft = false;
    obj.controls.fire = false;
}

let phisikTime = 0;
let phisikWhile;

const dead = (objectNum, objectKiller) => {
    console.log('\x1b[31m%s\x1b[0m', `${objectKiller.name} kill the ${objects[objectNum].name}`);
    /*for(var j = 1; j < models[objects[object].model].sketh.length; j += 2) {
        var StartX = (Math.cos((models[objects[object].model].sketh[j]  [0] + objects[object].d) * 0.0175) * models[objects[object].model].sketh[j]  [1]) + objects[object].x;
        var StartY = (Math.sin((models[objects[object].model].sketh[j]  [0] + objects[object].d) * 0.0175) * models[objects[object].model].sketh[j]  [1]) + objects[object].y;
        var StopX = (Math.cos((models[objects[object].model].sketh[j - 1][0] + objects[object].d) * 0.0175) * models[objects[object].model].sketh[j - 1][1]) + objects[object].x;
        var StopY = (Math.sin((models[objects[object].model].sketh[j - 1][0] + objects[object].d) * 0.0175) * models[objects[object].model].sketh[j - 1][1]) + objects[object].y;
        var Dir = Diraction(StartX, StartY, StopX, StopY);
        var L = Long(StartX, StartY, StopX, StopY);
        var fakeDir = rand(-1, 1);
        var helj = {'type': 'dead'};
        helj.x = (StartX + StopX) / 2;
        helj.y = (StartY + StopY) / 2;
        var D = Diraction(objects[object].x, objects[object].x, helj.x, helj.y);
        var S = rand(2, 7);
        helj.sx = objects[object].sx + Math.cos(D) * S;
        helj.sy = objects[object].sy + Math.sin(D) * S;
        helj.d = Dir * 57.30;
        helj.s = L / 2;
        helj.sd = fakeDir;

        deads.push(helj);
    }*/

    // KillMessage(objectKiller, objects[object]);

    if (objects[objectNum].team === objectKiller.team)
        if (objectKiller.team !== 'gray') {
            objectKiller.team = 'gray';
            // if (objectKiller.controller === 'player')
            // Message('skull', 'Traitor!');
        }

    /*
    if (models[objects[object].model].decor !== undefined)
        for(var j = 1; j < models[objects[object].model].decor.length; j += 2) {
            var StartX = (Math.cos((models[objects[object].model].decor[j]  [0] + objects[object].d) * 0.0175) * models[objects[object].model].decor[j]  [1]) + objects[object].x;
            var StartY = (Math.sin((models[objects[object].model].decor[j]  [0] + objects[object].d) * 0.0175) * models[objects[object].model].decor[j]  [1]) + objects[object].y;
            var StopX = (Math.cos((models[objects[object].model].decor[j - 1][0] + objects[object].d) * 0.0175) * models[objects[object].model].decor[j - 1][1]) + objects[object].x;
            var StopY = (Math.sin((models[objects[object].model].decor[j - 1][0] + objects[object].d) * 0.0175) * models[objects[object].model].decor[j - 1][1]) + objects[object].y;
            var Dir = Diraction(StartX, StartY, StopX, StopY);
            var L = Long(StartX, StartY, StopX, StopY);
            var fakeDir = rand(-1, 1);
            var helj = {'type': 'dead'};
            helj.x = (StartX + StopX) / 2;
            helj.y = (StartY + StopY) / 2;
            var D = Diraction(objects[object].x, objects[object].x, helj.x, helj.y);
            var S = rand(2, 7);
            helj.sx = objects[object].sx + Math.cos(D) * S;
            helj.sy = objects[object].sy + Math.sin(D) * S;
            helj.d = Dir * 57.30;
            helj.s = L / 2;
            helj.sd = fakeDir;

            deads.push(helj);
        }

     */
    //objects.splice(object,1);

    objects[objectNum].clear = true;
};

const hit = (object, line, hot, objectKiller) => {
    const realHot = hot / 50;
    // const realLine = (line - 1) / 2;
    if (objects[object].health !== undefined) {
        objects[object].health[0] -= realHot;

        if (objects[object].health[0] < 0)
            dead(object, objectKiller);
    }
};

let lastSendMapTime = Date.now();

const phisik = one => {
    const saveTime = Date.now();

    /* plazma */

    for (let n = 0; n < plasma.length; ++n) {
        if (plasma[n].s < 20) {
            plasma.splice(n, 1); //|| (!StarShow(plasma[n].x, plasma[n].y))
            --n;
            continue;
        }

        if (!!plasma[n].w) {
            var k = Math.min(Math.max(Math.round(plasma[n].s / (quality / 100)), 1), 20);
            for (var tt = 0; tt < k; tt++) {

                for (var i = 0; i < objects.length; i++)
                    if (getDistance(plasma[n].x + Math.cos(plasma[n].d * 0.0175) * (plasma[n].s * 1.5), plasma[n].y + Math.sin(plasma[n].d * 0.0175) * (plasma[n].s * 1.5), objects[i].x, objects[i].y) < objects[i].max + plasma[n].s + 2)
                        for (var j = 1; j < models[objects[i].model].sketh.length; j += 2)
                            if (lineIntersection(plasma[n].x, plasma[n].y, plasma[n].x + Math.cos(plasma[n].d * 0.0175) * plasma[n].s, plasma[n].y + Math.sin(plasma[n].d * 0.0175) * plasma[n].s, (Math.cos((models[objects[i].model].sketh[j - 1][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j - 1][1]) + objects[i].x + objects[i].sx, (Math.sin((models[objects[i].model].sketh[j - 1][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j - 1][1]) + objects[i].y + objects[i].sy, (Math.cos((models[objects[i].model].sketh[j][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j][1]) + objects[i].x + objects[i].sx, (Math.sin((models[objects[i].model].sketh[j][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j][1]) + objects[i].y + objects[i].sy)) {
                                hit(i, j, plasma[n].s, plasma[n].starter);
                                var l = plasma[n].s;
                                while (lineIntersection(plasma[n].x, plasma[n].y, plasma[n].x + Math.cos(plasma[n].d * 0.0175) * l, plasma[n].y + Math.sin(plasma[n].d * 0.0175) * l, (Math.cos((models[objects[i].model].sketh[j - 1][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j - 1][1]) + objects[i].x + objects[i].sx, (Math.sin((models[objects[i].model].sketh[j - 1][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j - 1][1]) + objects[i].y + objects[i].sy, (Math.cos((models[objects[i].model].sketh[j][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j][1]) + objects[i].x + objects[i].sx, (Math.sin((models[objects[i].model].sketh[j][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j][1]) + objects[i].y + objects[i].sy)) {
                                    l -= (map(quality, 0, 100, 10, 1));
                                }
                                l -= (map(quality, 0, 100, 10, 1));
                                plasma[n].x += Math.cos(plasma[n].d * 0.0175) * l;
                                plasma[n].y += Math.sin(plasma[n].d * 0.0175) * l;

                                const svd = plasma[n].d;
                                const barrier_dir = getDirection((Math.cos((models[objects[i].model].sketh[j - 1][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j - 1][1]) + objects[i].x, (Math.sin((models[objects[i].model].sketh[j - 1][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j - 1][1]) + objects[i].y, (Math.cos((models[objects[i].model].sketh[j][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j][1]) + objects[i].x, (Math.sin((models[objects[i].model].sketh[j][0] + objects[i].d) * 0.0175) * models[objects[i].model].sketh[j][1]) + objects[i].y);
                                plasma[n].d = (barrier_dir * 57.30) + ((barrier_dir * 57.30) - svd);

                                // const r = Mirrow * rand(0,2);
                                // for (let h = 0; h < r; h++)
                                // 	fire(plasma[n].x,plasma[n].y,plasma[n].d+(rand(-50,50)/10),plasma[n].s*(1/r/2),plasma[n].t,false,plasma[n].starter);
                                // plasma[n].s=0;

                                fire(plasma[n].x, plasma[n].y, plasma[n].d + (rand(-50, 50) / 10), plasma[n].s * (1 / 3), plasma[n].t, false, plasma[n].starter);
                                plasma[n].s = 0;

                                break;
                            }

                plasma[n].x += Math.cos(plasma[n].d * 0.0175) * plasma[n].s * (1 / k);
                plasma[n].y += Math.sin(plasma[n].d * 0.0175) * plasma[n].s * (1 / k);

            }
            plasma[n].s *= 0.96;
        }
        plasma[n].w = true;
    }

    /* objects */
    for (let n = 0; n < objects.length; n++) {

        if (objects[n].max === 0) {
            let max = 0;
            let mass = 0;

            for (let j = 0; j < models[objects[n].model].sketh.length; j++) {
                max = Math.max(max, models[objects[n].model].sketh[j][1]);
                mass += models[objects[n].model].sketh[j][1];
            }

            mass /= models[objects[n].model].sketh.length;

            objects[n].max = max;
            objects[n].mass = mass;
        }


        objects[n].x += objects[n].sx;
        objects[n].y += objects[n].sy;
        objects[n].d += objects[n].sd;

        objects[n].sx -= objects[n].sx * (2 / objects[n].mass);
        objects[n].sy -= objects[n].sy * (2 / objects[n].mass);
        objects[n].sd -= objects[n].sd * (2 / objects[n].mass);


        if (objectPhisik[objects[n].type] !== undefined)
            objectPhisik[objects[n].type](n);
        controller(objects[n]);

    }

    for (let n = 0; n < objects.length; n++)
        if (objects[n].clear) {
            objects.splice(n, 1);
            for (const key of Object.keys(assoc)) {
                if (assoc[key] === n) {
                    assoc[key] = findFreeRocket(objects[n].team);
                    objects[assoc[key]].name = objects[n].name;
                    objects[assoc[key]].controller = 'player';
                }
                if (assoc[key] > n)
                    assoc[key]--;
            }
        }

    /*
for (let n = 0; n < particles.length; n++){
    for (let i = 0; i < objects.length; i++)
        if (Long(particles[n].x+particles[n].sx,particles[n].y+particles[n].sy,objects[i].x,objects[i].y) < objects[i].max+10)
        {
            for(let j = 1; j < models[objects[i].model].sketh.length; j+=2)
                if (Intersection(particles[n].x,particles[n].y,particles[n].x+particles[n].sx,particles[n].y+particles[n].sy,(Math.cos((models[objects[i].model].sketh[j-1][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j-1][1])+objects[i].x+objects[i].sx,(Math.sin((models[objects[i].model].sketh[j-1][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j-1][1])+objects[i].y+objects[i].sy,(Math.cos((models[objects[i].model].sketh[j][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j][1])+objects[i].x+objects[i].sx,(Math.sin((models[objects[i].model].sketh[j][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j][1])+objects[i].y+objects[i].sy))
                {
                    let spd = Math.sqrt(Math.pow(objects[i].sx-particles[n].sx,2)+Math.pow(objects[i].sy-particles[n].sy,2));
                    let barrier_dir = Diraction((Math.cos((models[objects[i].model].sketh[j-1][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j-1][1])+objects[i].x,(Math.sin((models[objects[i].model].sketh[j-1][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j-1][1])+objects[i].y,(Math.cos((models[objects[i].model].sketh[j][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j][1])+objects[i].x,(Math.sin((models[objects[i].model].sketh[j][0]+objects[i].d)*0.0175)*models[objects[i].model].sketh[j][1])+objects[i].y);
                    let par_dir = Diraction(particles[n].x,particles[n].y,particles[n].x+particles[n].sx,particles[n].y+particles[n].sy);
                    let newdir = barrier_dir + (barrier_dir - par_dir);

                    particles[n].sx = Math.cos(newdir)*spd;
                    particles[n].sy = Math.sin(newdir)*spd;
                    break;
                }
        }

    particles[n].x += particles[n].sx;
    particles[n].y += particles[n].sy;

    particles[n].sx -= particles[n].sx * particlesLife;
    particles[n].sy -= particles[n].sy * particlesLife;

    particles[n].size = particles[n].sz / (particles[n].ss / (Math.abs(particles[n].sx)+Math.abs(particles[n].sy)));
}

for (var n = 0; n < particles.length; n++)
    if ((Math.abs(particles[n].sx)+Math.abs(particles[n].sy) < 0.5) || (!StarShow(particles[n].x,particles[n].y)))
        particles.splice(n, 1);

for (var n = 0; n < deads.length; n++){
    deads[n].x += deads[n].sx;
    deads[n].y += deads[n].sy;
    deads[n].d += deads[n].sd;

    deads[n].sx -= deads[n].sx * 0.01;
    deads[n].sy -= deads[n].sy * 0.01;
    //deads[n].sd -= deads[n].sd * particlesLife;

    deads[n].s -= deads[n].s * particlesLife;
}

for (var n = 0; n < deads.length; n++)
    if (deads[n].s < 0.2)
        deads.splice(n, 1);

for (var n = 0; n < stars.length; n++)
{
    stars[n].x += objects[pilotObject].sx/stars[n].z;
    stars[n].y += objects[pilotObject].sy/stars[n].z;

    if((stars[n].x - animator.canvas.map.camera.x - 10) > animator.canvas.map.camera.w)
        stars[n].x -= animator.canvas.map.camera.w+objects[pilotObject].speed;
    if((stars[n].x - animator.canvas.map.camera.x + 10) < 0)
        stars[n].x += animator.canvas.map.camera.w+objects[pilotObject].speed;
    if((stars[n].y - animator.canvas.map.camera.y - 10) > animator.canvas.map.camera.h)
        stars[n].y -= animator.canvas.map.camera.h+objects[pilotObject].speed;
    if((stars[n].y - animator.canvas.map.camera.y + 10) < 0)
        stars[n].y += animator.canvas.map.camera.h+objects[pilotObject].speed;

    stars[n].v = StarShow(stars[n].x,stars[n].y);

}
*/

    if (Date.now() - lastSendMapTime > 20) {
        sendMap();
        lastSendMapTime = Date.now();
    }

    phisikTime = Date.now() - saveTime;
    if (!one) {
        phisikWhile = setTimeout(phisik, Math.max(phisikHZ - phisikTime, 1));
    }
};