import { objects, Box, findFreeRocket } from "./objects";
import { startEngine, started } from "./engine";
import { rand } from "./utils";
import { needPlayers } from "./consts";

export const assoc = {
	// undefined: 0
};

const hashGenerator = length => {
	const symbols = 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890';
	let rez = 'H';
	for (let i = 0; i < length; i++)
		rez += symbols[Math.floor(Math.random()*symbols.length)];
	return rez;
};

export const connectPlayer = data => {
	const hash = hashGenerator(20);

	/*
	const num = objects.push(new Player)-1;
	assoc[hash] = num;

	objects[assoc[hash]].name = data.name; //'Player' + num;
	objects[assoc[hash]].model = rand(0,1) ? 'Pilgrim' : 'Fly';
	objects[assoc[hash]].team = teams[rand(1, teams.length) - 1];

	if (Object.keys(assoc).length === needPlayers)
	{
		boxs();
		startEngine();
	}

	console.log('\x1b[32m%s\x1b[0m',`registered user ${data.name}`);
	return {hash, num: assoc[hash]};
	*/
	const team = rand(0,1) ? 'lime' : 'red';
	const num = findFreeRocket(team);

	assoc[hash] = num;

	objects[num].name = data.name;
	objects[num].controller = 'player';

	if (!started && Object.keys(assoc).length >= needPlayers)
	{
		//boxs();
		startEngine();
	}

	console.log('\x1b[32m%s\x1b[0m',`registered user ${data.name}`);
	return {hash, num};
};