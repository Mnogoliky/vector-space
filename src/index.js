import express from 'express';
import { createMap } from "./objects";
import { port } from "./consts";
import { consoleIP } from './IP';
import expressWsWrap from 'express-ws'

/* script */
console.log('\x1b[36m%s\x1b[0m', 'Starting');

const expressApp = express();

expressWsWrap(expressApp);

expressApp.listen(port, () => {
	console.log('\x1b[35m%s\x1b[0m',`Server running on port `, port);
	console.log(`http://localhost:${port}`);
	require('./accept');
	require('./ws');
});

export default expressApp;

consoleIP();

createMap();