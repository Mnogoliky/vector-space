import { objects } from "./objects";
import { plasma } from "./plasma";
import { assoc } from "./connector";
import expressApp from "./index";

expressApp.ws('/ws', (ws, req) => {
    console.log('\x1b[33m%s\x1b[0m','Connect player');
    socketList.push(new RegistrySocket(ws));
})

let socketList = [];

let fsave, fsave2, controller, type, ftime, ftime2, sd, mass, rest;
const compactObjects = () => {
    const result = [];

    for (const object of objects) {
        ({
            fsave,
            fsave2,
            controller,
            type,
            ftime,
            ftime2,
            sd,
            mass,
            ...rest
        } = object);

        result.push(rest)
    }

    return result;
};

const cleanupMap = () => {
    socketList = socketList.filter(obj => obj.active);
}

export const sendMap = () => {
    if (socketList.length !== 0) {
        const objects = compactObjects();

        for (const connection of socketList) {
            connection.ws.send(JSON.stringify({objects, plasma, num: assoc[connection.hash]}));
        }
    }
};

class RegistrySocket {
    constructor(ws) {
        this.hash = undefined;
        this.active = true;
        this.ws = ws;

        this.ws.on('message', message => {
            const { hash, controls } = JSON.parse(message);
            const num = assoc[hash];
            this.hash = hash;
            objects[num].controls = controls;
        });

        this.ws.on('close', () => {
            console.log('\x1b[33m%s\x1b[0m','Disconnect player');
            const index = socketList.indexOf(this);

            if (index !== -1) {
                socketList.splice(index, 1);
            } else {
                this.active = false;
                cleanupMap();
            }
        });
    }
}