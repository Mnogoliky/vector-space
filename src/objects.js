import {rocketModels, teams} from "./consts";
import {rand} from "./utils";
import {getIsNotUnderObject} from "./Geometry";

export const objects = [
    {
        'controls': {},
        'controller': 'turel',
        'team': 'red',
        'name': 'THOR 1',
        'type': 'turel',
        'x': 2000,
        'y': 4000,
        'd': 1000,
        'sx': 0,
        'sy': 0,
        'sd': 0,
        'max': 0,
        'ftime': 500,
        fsave: Date.now(),
        'fire': false,
        'model': 'Torrelle',
        'health': [50, 50],
        'resurs': {'energy': [200, 200]}
    },
    {
        'controls': {},
        'controller': 'turel',
        'team': 'lime',
        'name': 'THOR 3',
        'type': 'turel',
        'x': -2000,
        'y': 4000,
        'd': 1000,
        'sx': 0,
        'sy': 0,
        'sd': 0,
        'max': 0,
        'ftime': 500,
        fsave: Date.now(),
        'fire': false,
        'model': 'Torrelle',
        'health': [100, 100],
        'resurs': {'energy': [200, 200]}
    },
    {
        'controls': {},
        'controller': 'turel',
        'team': 'white',
        'name': 'Custom',
        'type': 'turel',
        'x': 0,
        'y': 5000,
        'd': 1000,
        'sx': 0,
        'sy': 0,
        'sd': 0,
        'max': 0,
        'ftime': 0,
        fsave: Date.now(),
        'fire': false,
        'model': 'Torrelle',
        'health': [30, 30],
        'resurs': {'energy': [500, 500]}
    },
];

export const getRandomModel = () => {
    return rocketModels[rand(0, rocketModels.length - 1)]
}

export const getRandomTeam = () => {
    return teams[rand(0, teams.length - 1)]
}

class GameObject {
    constructor() {
        this.x = 0;
        this.y = 0;
        this.d = 0;
        this.sx = 0;
        this.sy = 0;
        this.sd = 0;
        this.max = 0;
        this.fsave = Date.now();
        this.fsave2 = Date.now();
    }
}

export class Player extends GameObject {
    constructor() {
        super();

        this.controls = {};
        this.controller = null;
        this.team = getRandomTeam();
        this.name = 'Empty';
        this.type = 'rocket';
        this.speed = rand(20, 30);
        this.model = getRandomModel();
        this.ftime = rand(50, 250);
        this.ftime2 = this.model === 'Pilgrim' ? rand(0, 100) : rand(1000, 3000);

        const maxHealth = this.model === 'Pilgrim' ? rand(400, 600) : rand(100, 300);
        const maxFuel = this.model === 'Pilgrim' ? rand(500, 900) : rand(100, 300);
        const maxEnergy = this.model === 'Pilgrim' ? rand(500, 900) : rand(100,300);

        this.health = [maxHealth, maxHealth];
        this.resurs = {fuel: [maxFuel, maxFuel], energy: [maxEnergy, maxEnergy]}
    }
}

export class Box extends GameObject {
    constructor() {
        super();
        this.controls = {};
        this.controller = null;
        this.team = getRandomTeam();
        this.name = 'Box';
        this.type = 'block';
        this.speed = 20;
        this.ftime = 40;
        this.ftime2 = 40;
        this.model = 'Block';
        this.health = [50, 50];
        this.resurs = {fuel: [100, 100], energy: [10, 10]}
    }
}


export const findFreeRocket = team => {
    for (let i = 0; i < objects.length; ++i) {
        const obj = objects[i];
        if (
            obj.controller === null &&
            obj.type === 'rocket' &&
            obj.team === team
        ) {
            return i;
        }
    }

    return objects.push(new Player()) - 1;
};

const createBoxes = () => {
    const x = 0;
    const y = 0;
    const W = 5000;
    const H = 5000;

    for (let i = 0; i < 5; i++) {
        const currentX = rand(x - W, x + W);
        const currentY = rand(y - H, y + H);

        if (!getIsNotUnderObject(currentX, currentY)) {
            --i;
            continue;
        }

        const box = new Box();
        box.x = currentX;
        box.y = currentY;
        box.d = rand(0, 360);
        objects.push(box);
    }
};


export const createMap = () => {
    const betweenTeams = 3500;
    const betweenRockets = 1000;

    createBoxes();

    let rocket;
    for (let i = 0; i < 5; ++i)
        for (let j = 0; j < 5; ++j) {

            rocket = new Player;
            rocket.x = (-betweenTeams) - (betweenRockets * i);
            rocket.y = (betweenRockets * j);
            rocket.d = 0;
            objects.push(rocket);

            rocket = new Player;
            rocket.x = (betweenTeams) + (betweenRockets * i);
            rocket.y = (betweenRockets * j);
            rocket.d = 180;
            objects.push(rocket);
        }
};